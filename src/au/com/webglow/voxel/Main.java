package au.com.webglow.voxel;

import java.io.IOException;

import au.com.webglow.voxel.analyse.FrameAnalyser;
import au.com.webglow.voxel.analyse.flash.GridBlockCoordinateStorage;
import au.com.webglow.voxel.gui.DebugConsoleFrame;

public class Main {
	private static final byte[] READY = new byte[] {1};

	public static void main(String[] args) {
		try {
			DebugConsoleFrame.showFrame();
			GridBlockCoordinateStorage.init();
			FrameAnalyser frameAnalyser = FrameAnalyser.newInstance();
			StreamDataReader streamDataReader = StreamDataReader.newBufferReadingInstance(frameAnalyser);
			notifyJavaIsReady();
			streamDataReader.beginReading();
		} catch (Exception e) {
			DebugConsoleFrame.appendLogTextLn(e);
		}
	}

	private static void notifyJavaIsReady() throws IOException {
		System.out.write(READY);  // Indicate to c++ that application is ready
		DebugConsoleFrame.appendLogTextLn("Notified Ready");
	}
}
