package au.com.webglow.voxel.analyse.flash;

import static au.com.webglow.voxel.Constants.DEPTH_COUNT;
import static au.com.webglow.voxel.Constants.GRID_BLOCK_X_COUNT;
import static au.com.webglow.voxel.Constants.GRID_BLOCK_Y_COUNT;
import static au.com.webglow.voxel.Constants.GRID_BLOCK_Z_COUNT;
import static au.com.webglow.voxel.Constants.HIGH_ACCURACY_DEPTH_COUNT;
import static au.com.webglow.voxel.Constants.HIGH_ACCURACY_GRID_BLOCK_X_COUNT;
import static au.com.webglow.voxel.Constants.HIGH_ACCURACY_GRID_BLOCK_Y_COUNT;
import static au.com.webglow.voxel.Constants.HIGH_ACCURACY_GRID_BLOCK_Z_COUNT;

import au.com.webglow.voxel.analyse.CalculationUtil;
import au.com.webglow.voxel.analyse.units.GridBlockCoordinate;
import au.com.webglow.voxel.analyse.units.GridBlockPosition;
import au.com.webglow.voxel.analyse.units.HighAccuracyGridBlockCoordinate;
import au.com.webglow.voxel.gui.DebugConsoleFrame;

public class GridBlockCoordinateStorage {
	private static GridBlockCoordinateStorage instance;
	private GridBlockCoordinate[][][] pinholeCoordinates = new GridBlockCoordinate[239][319][DEPTH_COUNT];
	private HighAccuracyGridBlockCoordinate[][][] pinholeHighAccuracyCoordinates = new HighAccuracyGridBlockCoordinate[239][319][HIGH_ACCURACY_DEPTH_COUNT];
	private GridBlockPosition[][][] gridBlockPositions = new GridBlockPosition[GRID_BLOCK_X_COUNT][GRID_BLOCK_Y_COUNT][GRID_BLOCK_Z_COUNT];
	private GridBlockPosition[][][] highAccuracyGridBlockPositions = new GridBlockPosition[HIGH_ACCURACY_GRID_BLOCK_X_COUNT][HIGH_ACCURACY_GRID_BLOCK_Y_COUNT][HIGH_ACCURACY_GRID_BLOCK_Z_COUNT];
	
	public synchronized static void init() {
		if (instance == null) {
			instance = new GridBlockCoordinateStorage();
			instance.populate();
		}
	}
	
	public synchronized static GridBlockCoordinateStorage getInstance() {
		return instance;
	}
	
	private GridBlockCoordinateStorage() {}
	
	private void populate() {
		try {
			populatePinholeCoordinates();
			populatePinholeHighAccuracyCoordinates();
			populateGridBlockPositions();
			populateHighAccuracyGridBlockPositions();
		} catch (Exception e) {
			DebugConsoleFrame.appendLogTextLn(e);	
		}
	}

	private void populatePinholeCoordinates() {
		for (int y = 0; y < 239; y++) {
			for (int x = 0; x < 319; x++) {
				for (int d = 0; d < DEPTH_COUNT; d++) {
					pinholeCoordinates[y][x][d] = CalculationUtil.performPinholeCalculation(y, x, d);
				}
			}
		}
	}
	
	private void populatePinholeHighAccuracyCoordinates() {
		for (int y = 0; y < 239; y++) {
			for (int x = 0; x < 319; x++) {
				for (int d = 0; d < HIGH_ACCURACY_DEPTH_COUNT; d++) {
					pinholeHighAccuracyCoordinates[y][x][d] = CalculationUtil.performPinholeHighAccuracyCalculation(y, x, d);
				}
			}
		}
	}
	
	private void populateGridBlockPositions() {
		for (int blockX = 0; blockX < GRID_BLOCK_X_COUNT; blockX++) {
			for (int blockY = 0; blockY < GRID_BLOCK_Y_COUNT; blockY++) {
				for (int blockZ = 0; blockZ < GRID_BLOCK_Z_COUNT; blockZ++) {
					short z = CalculationUtil.calcZCoordFromBlockZ(blockZ);
					short x = CalculationUtil.calcXCoordFromBlockX(blockX);
					short y = CalculationUtil.calcYCoordFromBlockY(blockY);
					gridBlockPositions[blockX][blockY][blockZ] = GridBlockPosition.newInstance(z, x, y);
				}
			}
		}
	}
	
	private void populateHighAccuracyGridBlockPositions() {
		for (int highAccuracyBlockX = 0; highAccuracyBlockX < HIGH_ACCURACY_GRID_BLOCK_X_COUNT; highAccuracyBlockX++) {
			for (int highAccuracyBlockY = 0; highAccuracyBlockY < HIGH_ACCURACY_GRID_BLOCK_Y_COUNT; highAccuracyBlockY++) {
				for (int highAccuracyBlockZ = 0; highAccuracyBlockZ < HIGH_ACCURACY_GRID_BLOCK_Z_COUNT; highAccuracyBlockZ++) {
					short z = CalculationUtil.calcZCoordFromHighAccuracyBlockZ(highAccuracyBlockZ);
					short x = CalculationUtil.calcXCoordFromHighAccuracyBlockX(highAccuracyBlockX);
					short y = CalculationUtil.calcYCoordFromHighAccuracyBlockY(highAccuracyBlockY);
					highAccuracyGridBlockPositions[highAccuracyBlockX][highAccuracyBlockY][highAccuracyBlockZ] = GridBlockPosition.newInstance(z, x, y);
				}
			}
		}	
	}
	
	public GridBlockCoordinate getPinholeCoordinate(int y, int x, int d) {
		return pinholeCoordinates[y][x][d];
	}
	
	public HighAccuracyGridBlockCoordinate getHighAccuracyPinholeCoordinate(int y, int x, int d) {
		return pinholeHighAccuracyCoordinates[y][x][d];
	}
	
	public GridBlockPosition getGridBlockPosition(int x, int y, int z) {
		return gridBlockPositions[x][y][z];
	}

	public GridBlockPosition getHighAccuracyGridBlockPosition(int highAccuracyBlockX, int highAccuracyBlockY, int highAccuracyBlockZ) {
		return highAccuracyGridBlockPositions[highAccuracyBlockX][highAccuracyBlockY][highAccuracyBlockZ];
	}
}
