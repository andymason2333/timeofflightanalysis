package au.com.webglow.voxel.analyse;

import au.com.webglow.voxel.FrameCounter;
import au.com.webglow.voxel.PipeGridPositions;
import au.com.webglow.voxel.analyse.history.FloorGridManager;
import au.com.webglow.voxel.analyse.history.FrameDepthsManager;
import au.com.webglow.voxel.analyse.history.GridBlockPositionsManager;
import au.com.webglow.voxel.analyse.history.OccupiedCondensedGridManager;
import au.com.webglow.voxel.gui.DebugConsoleFrame;

public class FrameAnalyser {
	public static FrameDepthsManager frameDepthsManager;
	private static FloorGridManager floorGridManager;
	private static GridBlockPositionsManager gridBlockPositionsManager;
	private static OccupiedCondensedGridManager occupiedCondensedGridManager;
	
	public static FrameAnalyser newInstance() {
		FrameAnalyser retVal = new FrameAnalyser();
		retVal.initManagers();
		return retVal;
	}
	
	private void initManagers() {
		frameDepthsManager = new FrameDepthsManager();
		floorGridManager = new FloorGridManager();
		gridBlockPositionsManager = new GridBlockPositionsManager();
		occupiedCondensedGridManager = new OccupiedCondensedGridManager();
	}

	public void processFrame(byte[] frame) {
				
		try {
			FrameCounter.incrementFrameCount();
			frameDepthsManager.findDepthsForFrame(frame);
			gridBlockPositionsManager.findOccupiedGridBlockPositions(frameDepthsManager.getFramePixelHistory());
			gridBlockPositionsManager.processGridBlockOccupiedState();
			floorGridManager.findOccupiedFloorRegions(gridBlockPositionsManager.getGridBlockPositions());
			occupiedCondensedGridManager.processGridPositions(gridBlockPositionsManager.getGridBlockPositions());
			
			floorGridManager.processObjectDetectedGridBlocks(gridBlockPositionsManager.getGridBlockPositions());
			floorGridManager.findForegroundObjects(gridBlockPositionsManager.getGridBlockPositions());
			
//			occupiedCondensedGridManager.condenseHighAccuracyMovementRegions(gridBlockPositionsManager.getHighAccuracyGridBlocks(), gridBlockPositionsManager.getHighAccuracyRegions());
//			highAccuracyObjectManager.buildHighAccuracyObjectRegions(occupiedCondensedGridManager.getCondensedGridBlocks(), gridBlockPositionsManager.getHighAccuracyRegions());
//			floorGridManager.locateHighAccuracyObjectPositions(highAccuracyObjectManager.getHighAccuracyObjectRegions());
//			gridBlockPositionsManager.findVerticalAveragedPoints(highAccuracyObjectManager.getHighAccuracyObjectRegions(), occupiedCondensedGridManager.getCondensedGridBlocks());
			
			PipeGridPositions.pipeGridPositions(gridBlockPositionsManager.getGridBlockPositions());
			DebugConsoleFrame.update2dFrameDisplay(frameDepthsManager.getFramePixelHistory(), gridBlockPositionsManager.getGridBlockPositions());
			DebugConsoleFrame.updateGridBlockDistributionDisplay(gridBlockPositionsManager.getGridBlockPositions());
			
			frameDepthsManager.reset();
			floorGridManager.reset();
			gridBlockPositionsManager.reset();
			occupiedCondensedGridManager.reset();

		} catch (Exception e) {
			DebugConsoleFrame.appendLogTextLn(e);			
		}				
	}
}
