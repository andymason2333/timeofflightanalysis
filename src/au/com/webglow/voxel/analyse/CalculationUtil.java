package au.com.webglow.voxel.analyse;

import static au.com.webglow.voxel.Constants.DEPTH_BLOCK_LENGTH;
import static au.com.webglow.voxel.Constants.DEPTH_SCALE;
import static au.com.webglow.voxel.Constants.GRID_BLOCK_X_COUNT;
import static au.com.webglow.voxel.Constants.GRID_BLOCK_Y_COUNT;
import static au.com.webglow.voxel.Constants.GRID_BLOCK_Z_COUNT;
import static au.com.webglow.voxel.Constants.GRID_CM_PER_BLOCK;
import static au.com.webglow.voxel.Constants.HIGH_ACCURACY_DEPTH_BLOCK_LENGTH;
import static au.com.webglow.voxel.Constants.HIGH_ACCURACY_DEPTH_SCALE;
import static au.com.webglow.voxel.Constants.HIGH_ACCURACY_GRID_BLOCK_X_COUNT;
import static au.com.webglow.voxel.Constants.HIGH_ACCURACY_GRID_BLOCK_Y_COUNT;
import static au.com.webglow.voxel.Constants.HIGH_ACCURACY_GRID_BLOCK_Z_COUNT;
import static au.com.webglow.voxel.Constants.HIGH_ACCURACY_GRID_CM_PER_BLOCK;
import static au.com.webglow.voxel.Constants.HIGH_ACCURACY_PER_GRID_BLOCK;
import static au.com.webglow.voxel.Constants.OCCUPIED_CONDENSED_SIZE;
import static au.com.webglow.voxel.Constants.OCCUPIED_CONDENSED_Z_COUNT;
import static au.com.webglow.voxel.Constants.SENSOR_COL_SIZE;
import static au.com.webglow.voxel.Constants.SENSOR_ROW_SIZE;
import static au.com.webglow.voxel.analyse.units.FrameDepthChange.FURTHER;
import static au.com.webglow.voxel.analyse.units.FrameDepthChange.NEARER;
import static au.com.webglow.voxel.analyse.units.FrameDepthChange.SAME;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

import au.com.webglow.voxel.Constants;
import au.com.webglow.voxel.analyse.units.FrameDepthChange;
import au.com.webglow.voxel.analyse.units.FramePixelDepths;
import au.com.webglow.voxel.analyse.units.FramePixelHistory;
import au.com.webglow.voxel.analyse.units.GridBlockCoordinate;
import au.com.webglow.voxel.analyse.units.GridBlockPosition;
import au.com.webglow.voxel.analyse.units.HighAccuracyGridBlockCoordinate;

public class CalculationUtil {
	public final static double SENSOR_PIXEL_SCALE = 984 / 319;   // Max view width(cm) divide col count
	private static int[][] frameDepths = new int[SENSOR_ROW_SIZE][SENSOR_COL_SIZE];
	private static int[][] highAccuracyFrameDepths = new int[SENSOR_ROW_SIZE][SENSOR_COL_SIZE];
	
	public static GridBlockCoordinate performPinholeCalculation(int row, int col, int depth) {
		short z = (short)(depth * DEPTH_BLOCK_LENGTH);
		short x = (short)((((319 / 2) - col) * SENSOR_PIXEL_SCALE) * ((double)z / 800));
		short y = (short)((((239 / 2) - row) * SENSOR_PIXEL_SCALE) * ((double)z / 800));	
		
		int blockZ = (int)Math.floor((double)z / GRID_CM_PER_BLOCK);
		int blockX = (GRID_BLOCK_X_COUNT / 2) + (int)Math.floor((double)x / GRID_CM_PER_BLOCK);
		int blockY = (GRID_BLOCK_Y_COUNT / 2) + (int)Math.floor((double)y / GRID_CM_PER_BLOCK);
		
		if (blockX >= 0 && blockY >= 0 && blockZ >= 0
				&& blockX < GRID_BLOCK_X_COUNT && blockY < GRID_BLOCK_Y_COUNT && blockZ < GRID_BLOCK_Z_COUNT) {
			return GridBlockCoordinate.newInstance(blockZ, blockX, blockY);	
		} else {
			return null;
		}
	}
	
	public static HighAccuracyGridBlockCoordinate performPinholeHighAccuracyCalculation(int row, int col, int depth) {
		short z = (short)(depth * HIGH_ACCURACY_DEPTH_BLOCK_LENGTH);
		short x = (short)((((319 / 2) - col) * SENSOR_PIXEL_SCALE) * ((double)z / 800));
		short y = (short)((((239 / 2) - row) * SENSOR_PIXEL_SCALE) * ((double)z / 800));	
		
		int highAccuracyBlockZ = (int)Math.floor((double)z / HIGH_ACCURACY_GRID_CM_PER_BLOCK);
		int highAccuracyBlockX = (HIGH_ACCURACY_GRID_BLOCK_X_COUNT / 2) + (int)Math.floor((double)x / HIGH_ACCURACY_GRID_CM_PER_BLOCK);
		int highAccuracyBlockY = (HIGH_ACCURACY_GRID_BLOCK_Y_COUNT / 2) + (int)Math.floor((double)y / HIGH_ACCURACY_GRID_CM_PER_BLOCK);
		
		if (highAccuracyBlockX >= 0 && highAccuracyBlockY >= 0 && highAccuracyBlockZ >= 0
				&& highAccuracyBlockX < HIGH_ACCURACY_GRID_BLOCK_X_COUNT && highAccuracyBlockY < HIGH_ACCURACY_GRID_BLOCK_Y_COUNT && highAccuracyBlockZ < HIGH_ACCURACY_GRID_BLOCK_Z_COUNT) {

			int blockZ = highAccuracyBlockZ/HIGH_ACCURACY_PER_GRID_BLOCK;
			int blockX = highAccuracyBlockX/HIGH_ACCURACY_PER_GRID_BLOCK;
			int blockY = highAccuracyBlockY/HIGH_ACCURACY_PER_GRID_BLOCK;
			int highAccuracyIndexZ = highAccuracyBlockZ % HIGH_ACCURACY_PER_GRID_BLOCK;
			int highAccuracyIndexX = highAccuracyBlockX % HIGH_ACCURACY_PER_GRID_BLOCK;
			int highAccuracyIndexY = highAccuracyBlockY % HIGH_ACCURACY_PER_GRID_BLOCK;
			
			return HighAccuracyGridBlockCoordinate.newInstance(blockZ, blockX, blockY, highAccuracyIndexZ, highAccuracyIndexX, highAccuracyIndexY);	
		} else {
			return null;
		}
	}
	
	public static FramePixelDepths calcBlockFrameDepths(byte[] frame, FramePixelHistory[][] framePixelHistory) {
		FramePixelDepths retVal = new FramePixelDepths();
		int bytePosition = 0;
		for (int row = 0; row < SENSOR_ROW_SIZE; row++) {
			for (int col = 0; col < SENSOR_COL_SIZE; col++) {			
				byte[] depthIntArray = Arrays.copyOfRange(frame, bytePosition, (bytePosition+=2));								
				short depth = ByteBuffer.wrap(depthIntArray).order(ByteOrder.BIG_ENDIAN).getShort();
				
				int depthIndex = (int) (depth * DEPTH_SCALE);
				if (depthIndex >= Constants.DEPTH_COUNT) {
					depthIndex = Constants.DEPTH_COUNT-1;
				}
				frameDepths[row][col] = depthIndex;
				
				int highAccuracyDepthIndex = (int) (depth * HIGH_ACCURACY_DEPTH_SCALE);
				if (highAccuracyDepthIndex >= Constants.HIGH_ACCURACY_DEPTH_COUNT) {
					highAccuracyDepthIndex = Constants.HIGH_ACCURACY_DEPTH_COUNT-1;
				}
				highAccuracyFrameDepths[row][col] = highAccuracyDepthIndex;
			}
		}
		retVal.setFrameDepths(frameDepths);
		retVal.setHighAccuracyFrameDepths(highAccuracyFrameDepths);
		retVal.setFrameDepthChanges(calcBlockFrameDepthChanges(frameDepths, framePixelHistory));
		return retVal;
	}
	
	private static FrameDepthChange[][] calcBlockFrameDepthChanges(int[][] frameDepths, FramePixelHistory[][] framePixelHistory) {
		FrameDepthChange[][] retVal = new FrameDepthChange[SENSOR_ROW_SIZE][SENSOR_COL_SIZE];
		for (int row = 0; row < SENSOR_ROW_SIZE; row++) {
			for (int col = 0; col < SENSOR_COL_SIZE; col++) {
				int depth = frameDepths[row][col];
				int previousDepth = framePixelHistory[row][col].getPreviousFrameDepth();
				if (framePixelHistory[row][col].getPreviousFrameDepth() != -1) {
					if (depth == previousDepth) {
						retVal[row][col] = SAME;
					} else if (depth > previousDepth) {
						retVal[row][col] = FURTHER;
					} else if (depth < previousDepth) {
						retVal[row][col] = NEARER;
					}
				} else {
					retVal[row][col] = SAME;
				}
			}
		}
		return retVal;
	}
	
	public static int calcCondensedBlockLowIndex(int index) {
		return Math.max(0, index - (OCCUPIED_CONDENSED_SIZE-1));
	}

	public static int calcCondensedBlockHighIndex(int index) {
		return Math.min((OCCUPIED_CONDENSED_Z_COUNT-1), index);
	}

	public static int calcGridBlockLowIndex(int condensedIndex) {
		return condensedIndex;
	}

	public static int calcGridBlockHighIndex(int condensedIndex) {
		return condensedIndex + (OCCUPIED_CONDENSED_SIZE-1);
	}

	public static short calcZCoordFromBlockZ(int blockZ) {
		return (short)((blockZ * GRID_CM_PER_BLOCK) + (GRID_CM_PER_BLOCK / 2));
	}

	public static short calcXCoordFromBlockX(int blockX) {
		return (short)(((blockX - (GRID_BLOCK_X_COUNT / 2) ) * GRID_CM_PER_BLOCK) + (GRID_CM_PER_BLOCK / Math.copySign(2, blockX)));
	}

	public static short calcYCoordFromBlockY(int blockY) {
		return (short)(((blockY - (GRID_BLOCK_Y_COUNT / 2) ) * GRID_CM_PER_BLOCK) + (GRID_CM_PER_BLOCK / Math.copySign(2, blockY)));
	}

	public static short calcZCoordFromHighAccuracyBlockZ(int blockZ) {
		return (short)((blockZ * HIGH_ACCURACY_GRID_CM_PER_BLOCK) + (HIGH_ACCURACY_GRID_CM_PER_BLOCK / 2));
	}

	public static short calcXCoordFromHighAccuracyBlockX(int blockX) {
		return (short)(((blockX - (HIGH_ACCURACY_GRID_BLOCK_X_COUNT / 2) ) * HIGH_ACCURACY_GRID_CM_PER_BLOCK) + (HIGH_ACCURACY_GRID_CM_PER_BLOCK / Math.copySign(2, blockX)));
	}

	public static short calcYCoordFromHighAccuracyBlockY(int blockY) {
		return (short)(((blockY - (HIGH_ACCURACY_GRID_BLOCK_Y_COUNT / 2) ) * HIGH_ACCURACY_GRID_CM_PER_BLOCK) + (HIGH_ACCURACY_GRID_CM_PER_BLOCK / Math.copySign(2, blockY)));
	}
	
	public static float calculateFloorGridGradient(int x, int z) {
		int halfX = (GRID_BLOCK_X_COUNT/2);
		if (x == halfX) {
			return 0;
		} else {
			return (float)(x-halfX) / (float)z;
		}
	}
	
	public static int calculateFloorGridDistanceToCamera(int x, int z) {
		int halfX = (GRID_BLOCK_X_COUNT/2);
		if (x == halfX) {
			return z;
		} else if (x < halfX) {
			return (int) Math.sqrt(Math.pow((halfX-x), 2) + Math.pow(z, 2));
		} else {
			return (int) Math.sqrt(Math.pow((x-halfX), 2) + Math.pow(z, 2));
		}
	}

	public static byte[] calcGridBlockPositionPipeBuffer(GridBlockPosition gridBlockPosition) {
		byte[] retVal = new byte[6];
		ByteBuffer positionsCountBuffer = ByteBuffer.allocate(2);
		positionsCountBuffer.order(ByteOrder.BIG_ENDIAN);
		positionsCountBuffer.putShort(gridBlockPosition.getZ());
		byte[] zArr = positionsCountBuffer.array();
		retVal[0] = zArr[0];
		retVal[1] = zArr[1];
		
		positionsCountBuffer = ByteBuffer.allocate(2);
		positionsCountBuffer.order(ByteOrder.BIG_ENDIAN);
		positionsCountBuffer.putShort(gridBlockPosition.getX());
		byte[] xArr = positionsCountBuffer.array();
		retVal[2] = xArr[0];
		retVal[3] = xArr[1];
		
		positionsCountBuffer = ByteBuffer.allocate(2);
		positionsCountBuffer.order(ByteOrder.BIG_ENDIAN);
		positionsCountBuffer.putShort(gridBlockPosition.getY());
		byte[] yArr = positionsCountBuffer.array();
		retVal[4] = yArr[0];
		retVal[5] = yArr[1];
		return retVal;
	}
}
