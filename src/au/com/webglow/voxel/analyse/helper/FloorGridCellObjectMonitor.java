package au.com.webglow.voxel.analyse.helper;

import au.com.webglow.voxel.analyse.units.MinMaxValues;

public class FloorGridCellObjectMonitor {
	private boolean objectDetectedHighlighted = false;
	private boolean highAccuracyHighlighted = false;
	private int highAccuracyOccupiedPointCount = 0;
	private int highAccuracyEmptyPointCount = 0;
	private MinMaxValues minMaxDetectedBlockHeights = new MinMaxValues();
	
	public void processObjectDetectedBlock(int yHeight) {
		objectDetectedHighlighted = true;
		minMaxDetectedBlockHeights.processValue(yHeight);
	}
	
	public void processOccupiedCondensedHighAccuracyGridBlock(int occupiedCount, int emptyCount) {
		highAccuracyHighlighted = true;
		highAccuracyOccupiedPointCount = occupiedCount;
		highAccuracyEmptyPointCount = emptyCount;
	}
	
	public boolean isObjectDetectedHighlighted() {
		return objectDetectedHighlighted;
	}

	public boolean isHighAccuracyHighlighted() {
		return highAccuracyHighlighted;
	}

	public int getHighAccuracyOccupiedPointCount() {
		return highAccuracyOccupiedPointCount;
	}

	public int getHighAccuracyEmptyPointCount() {
		return highAccuracyEmptyPointCount;
	}

	public MinMaxValues getMinMaxDetectedBlockHeights() {
		return minMaxDetectedBlockHeights;
	}

	public void invalidateHighlights() {
		reset();
	}
	
	public void reset() {
		objectDetectedHighlighted = false;
		highAccuracyHighlighted = false;
		highAccuracyOccupiedPointCount = 0;
		highAccuracyEmptyPointCount = 0;
		minMaxDetectedBlockHeights.reset();
	}
}
