package au.com.webglow.voxel.analyse.helper;

import static au.com.webglow.voxel.analyse.helper.AbleToFormNewlyOccupiedStatus.ABLE_TO_FORM_NEWLY_OCCUPIED;

public class GridBlockNewlyOccupiedMonitor {
	private static final int REQUIRED_NON_OCCUPIED_FRAME_COUNT = 30;
	private static final int MAINTAIN_NON_OCCUPIED_DURATION = 30;
	private static final int MAINTAIN_NEWLY_OCCUPIED_DURATION = 30;
	private int continuousNonOccupiedCount = 0;
	private int frameSinceBecameNonOccupied = -1;
	private int frameSinceBecameNewlyOccupied = -1;
	private boolean objectDetected = false;
	
	public GridBlockNewlyOccupiedMonitor() {}
	
	public void processOccupiedValue(boolean occupied, AbleToFormNewlyOccupiedStatus ableToFormNewlyOccupiedStatus) {
		if (!occupied) {
			if (++continuousNonOccupiedCount >= REQUIRED_NON_OCCUPIED_FRAME_COUNT) {
				frameSinceBecameNonOccupied = 0;
			}
		} else {
			continuousNonOccupiedCount = 0;
			if (ableToFormNewlyOccupiedStatus == ABLE_TO_FORM_NEWLY_OCCUPIED && frameSinceBecameNonOccupied != -1) {
				frameSinceBecameNewlyOccupied = 0;
			}
		}
	}

	public boolean isNewlyOccupied() {
		return frameSinceBecameNewlyOccupied != -1;
	}
	
	public boolean isObjectDetected() {
		return objectDetected;
	}

	public void flagObjectDetected() {
		objectDetected = true;
	}

	public void prepareForNextFrame() {
		if (continuousNonOccupiedCount > REQUIRED_NON_OCCUPIED_FRAME_COUNT) {
			continuousNonOccupiedCount = REQUIRED_NON_OCCUPIED_FRAME_COUNT;
		}
		if (frameSinceBecameNonOccupied != -1 && ++frameSinceBecameNonOccupied > MAINTAIN_NON_OCCUPIED_DURATION) {
			frameSinceBecameNonOccupied = -1;
		}
		if (frameSinceBecameNewlyOccupied != -1 && ++frameSinceBecameNewlyOccupied > MAINTAIN_NEWLY_OCCUPIED_DURATION) {
			frameSinceBecameNewlyOccupied = -1;
		}
		objectDetected = false;
	}
}
