package au.com.webglow.voxel.analyse.helper;

import au.com.webglow.voxel.analyse.units.FrameDepthChange;

public class FrameDepthChangeMinimumDurationMonitor extends FrameDepthChangeMonitor {
	private FrameDepthChange[] acceptableDepthChanges; 
	private int minDuration;
	private int durationCounter = 0;

	public FrameDepthChangeMinimumDurationMonitor(FrameDepthChange[] acceptableDepthChanges, int minDuration) {
		super();
		this.acceptableDepthChanges = acceptableDepthChanges;
		this.minDuration = minDuration;
	}

	@Override
	ProcessFrameDepthChangeOutcome processFrameDepthChange(FrameDepthChange frameDepthChange, int frameDepthChangeAmount) {
		if (contains(acceptableDepthChanges, frameDepthChange)) {
			if (++durationCounter >= minDuration) {
				return ProcessFrameDepthChangeOutcome.VERIFIED;
			} else {
				return ProcessFrameDepthChangeOutcome.VERIFYING;
			}
		} else {
			durationCounter = 0;
			return ProcessFrameDepthChangeOutcome.INVALID;
		}
	}

	private boolean contains(FrameDepthChange[] acceptableDepthChanges, FrameDepthChange frameDepthChange) {
		for (FrameDepthChange acceptableChange: acceptableDepthChanges) {
			if (acceptableChange == frameDepthChange) {
				return true;
			}
		}
		return false;
	}

	@Override
	void reset() {
		durationCounter = 0;
	}
}
