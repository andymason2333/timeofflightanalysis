package au.com.webglow.voxel.analyse.helper;

import static au.com.webglow.voxel.analyse.helper.AbleToFormNewlyOccupiedStatus.ABLE_TO_FORM_NEWLY_OCCUPIED;
import static au.com.webglow.voxel.analyse.helper.AbleToFormNewlyOccupiedStatus.NOT_ABLE_TO_FORM_NEWLY_OCCUPIED;
import static au.com.webglow.voxel.analyse.helper.FramePixelMonitorState.INIT;
import static au.com.webglow.voxel.analyse.helper.FramePixelMonitorState.IN_BACKGROUND;
import static au.com.webglow.voxel.analyse.helper.FramePixelMonitorState.IN_FOREGROUND;
import static au.com.webglow.voxel.analyse.helper.FramePixelMonitorState.JUMPED_TO_FOREGROUND;
import static au.com.webglow.voxel.analyse.helper.ProcessFrameDepthChangeOutcome.INVALID;
import static au.com.webglow.voxel.analyse.helper.ProcessFrameDepthChangeOutcome.VERIFIED;
import static au.com.webglow.voxel.analyse.helper.ProcessFrameDepthChangeOutcome.VERIFYING;
import static au.com.webglow.voxel.analyse.units.FrameDepthChange.NEARER;
import static au.com.webglow.voxel.analyse.units.FrameDepthChange.SAME;

import au.com.webglow.voxel.analyse.units.FrameDepthChange;

public class FrameDepthChangeMonitorManager {
	private AbleToFormNewlyOccupiedStatus ableToFormNewlyOccupiedStatus = AbleToFormNewlyOccupiedStatus.NOT_ABLE_TO_FORM_NEWLY_OCCUPIED;
	private FramePixelMonitorState framePixelMonitorState = INIT;
	private FrameDepthChangeMonitor pixelInBackgroundMonitor = FrameDepthChangeMonitor.newMinimumDurationInstance(new FrameDepthChange[]{SAME}, 5);
	private FrameDepthChangeMonitor pixelJumpsToForegroundMonitor = FrameDepthChangeMonitor.newDepthShiftInstance(NEARER, 2);
	private FrameDepthChangeMonitor pixelInForegroundMonitor = FrameDepthChangeMonitor.newMinimumDurationInstance(new FrameDepthChange[]{NEARER, SAME}, 5);

	public static FrameDepthChangeMonitorManager newInstance() {
		return new FrameDepthChangeMonitorManager();
	}

	private FrameDepthChangeMonitorManager() {}
	
	public void processFrameDepthChange(FrameDepthChange frameDepthChange, int frameDepthChangeAmount) {
		FramePixelMonitorState newFramePixelMonitorState = null;
		switch (framePixelMonitorState) {
			case INIT:
				if (pixelInBackgroundMonitor.processFrameDepthChange(frameDepthChange, frameDepthChangeAmount) == VERIFIED) {
					newFramePixelMonitorState = IN_BACKGROUND;
				}
				break;
			case IN_BACKGROUND:
				if (pixelJumpsToForegroundMonitor.processFrameDepthChange(frameDepthChange, frameDepthChangeAmount) == VERIFIED) {
					newFramePixelMonitorState = JUMPED_TO_FOREGROUND;
				} else if (pixelInBackgroundMonitor.processFrameDepthChange(frameDepthChange, frameDepthChangeAmount) == INVALID) {
					newFramePixelMonitorState = INIT;
				}
				break;
			case JUMPED_TO_FOREGROUND:
				ProcessFrameDepthChangeOutcome outcome = pixelInForegroundMonitor.processFrameDepthChange(frameDepthChange, frameDepthChangeAmount);
				if (outcome == VERIFYING) {
					newFramePixelMonitorState = JUMPED_TO_FOREGROUND;
				} else if (outcome == VERIFIED) {
					newFramePixelMonitorState = IN_FOREGROUND;
				} else if (outcome == INVALID) {
					newFramePixelMonitorState = INIT;
				}
				break;
			case IN_FOREGROUND:
				if (pixelInForegroundMonitor.processFrameDepthChange(frameDepthChange, frameDepthChangeAmount) == INVALID) {
					newFramePixelMonitorState = INIT;
				}
				break;
		}
		
		if (framePixelMonitorState != INIT && newFramePixelMonitorState == INIT) {
			pixelInBackgroundMonitor.reset();
			pixelJumpsToForegroundMonitor.reset();
			pixelInForegroundMonitor.reset();
		}
		
		if (framePixelMonitorState == IN_FOREGROUND) {
			ableToFormNewlyOccupiedStatus = ABLE_TO_FORM_NEWLY_OCCUPIED;
			newFramePixelMonitorState = INIT;
		} else {
			ableToFormNewlyOccupiedStatus = NOT_ABLE_TO_FORM_NEWLY_OCCUPIED;
		}
		
		if (newFramePixelMonitorState != null) {
			framePixelMonitorState = newFramePixelMonitorState;
		}
	}

	public AbleToFormNewlyOccupiedStatus getAbleToFormNewlyOccupiedStatus() {
		return ableToFormNewlyOccupiedStatus;
	}
}
