package au.com.webglow.voxel.analyse.helper;

public enum ProcessFrameDepthChangeOutcome {
	VERIFYING, VERIFIED, VERIFIED_VERIFYING, INVALID;
}
