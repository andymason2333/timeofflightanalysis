package au.com.webglow.voxel.analyse.helper;

import au.com.webglow.voxel.analyse.units.FrameDepthChange;

public abstract class FrameDepthChangeMonitor {

	public static FrameDepthChangeMonitor newMinimumDurationInstance(FrameDepthChange[] acceptableDepthChanges, int minDuration) {
		return new FrameDepthChangeMinimumDurationMonitor(acceptableDepthChanges, minDuration);
	}
	
	public static FrameDepthChangeMonitor newDepthShiftInstance(FrameDepthChange acceptableDepthChange, int minDepthChangeAmount) {
		return new FrameDepthChangeDepthShiftMonitor(acceptableDepthChange, minDepthChangeAmount);
	}

	protected FrameDepthChangeMonitor() {}

	abstract ProcessFrameDepthChangeOutcome processFrameDepthChange(FrameDepthChange frameDepthChange, int frameDepthChangeAmount);
	abstract void reset();
}
