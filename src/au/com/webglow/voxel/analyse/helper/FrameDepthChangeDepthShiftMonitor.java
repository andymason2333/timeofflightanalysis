package au.com.webglow.voxel.analyse.helper;

import au.com.webglow.voxel.analyse.units.FrameDepthChange;

public class FrameDepthChangeDepthShiftMonitor extends FrameDepthChangeMonitor {
	private FrameDepthChange acceptableDepthChange; 
	private int minDepthChangeAmount;

	public FrameDepthChangeDepthShiftMonitor(FrameDepthChange acceptableDepthChange, int minDepthChangeAmount) {
		super();
		this.acceptableDepthChange = acceptableDepthChange;
		this.minDepthChangeAmount = minDepthChangeAmount;
	}

	@Override
	ProcessFrameDepthChangeOutcome processFrameDepthChange(FrameDepthChange frameDepthChange, int frameDepthChangeAmount) {
		if (frameDepthChange == acceptableDepthChange) {
			if (frameDepthChange == FrameDepthChange.NEARER && Math.abs(frameDepthChangeAmount) >= minDepthChangeAmount) {
				return ProcessFrameDepthChangeOutcome.VERIFIED;
			} else if (frameDepthChange == FrameDepthChange.FURTHER && frameDepthChangeAmount >= minDepthChangeAmount) {
				return ProcessFrameDepthChangeOutcome.VERIFIED;
			}
		}
		return ProcessFrameDepthChangeOutcome.INVALID;
	}

	@Override
	void reset() {
		
	}
}
