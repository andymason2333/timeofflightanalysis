package au.com.webglow.voxel.analyse.helper;

public class FloorGridRegionOccupiedMonitor {
	private static final int REGION_SIZE = 2;
	private static final int MAX_FRAMES_SINCE_OCCUPIED = 30;
	private int[][] framesSinceOccupied = new int[REGION_SIZE][REGION_SIZE];

	public static FloorGridRegionOccupiedMonitor newInstance() {
		FloorGridRegionOccupiedMonitor retVal = new FloorGridRegionOccupiedMonitor();
		return retVal;
	}
	
	private FloorGridRegionOccupiedMonitor() {
		for (int x = 0; x < REGION_SIZE; x++) {
			for (int z = 0; z < REGION_SIZE; z++) {
				framesSinceOccupied[x][z] = -1;
			}	
		}
	}
	
	public void processNewlyOccupiedGridBlock(int x, int z, int y) {
		if (framesSinceOccupied[x][z] == -1) {
			framesSinceOccupied[x][z] = 0;
		}
	}
	
	public boolean isOccupied() {
		// 3 of the cells != -1 and different values
		// also keep tract of min max y
		return false;
	}
	
	public void reset() {
		for (int x = 0; x < REGION_SIZE; x++) {
			for (int z = 0; z < REGION_SIZE; z++) {
				if (framesSinceOccupied[x][z] != -1 && framesSinceOccupied[x][z] > MAX_FRAMES_SINCE_OCCUPIED) {
					framesSinceOccupied[x][z] = -1;	
				}
			}	
		}
	}
}
