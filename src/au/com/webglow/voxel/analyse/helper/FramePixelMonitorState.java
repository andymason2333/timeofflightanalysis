package au.com.webglow.voxel.analyse.helper;

public enum FramePixelMonitorState {
	INIT, IN_BACKGROUND, JUMPED_TO_FOREGROUND, IN_FOREGROUND;
}
