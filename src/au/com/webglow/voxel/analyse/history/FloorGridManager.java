package au.com.webglow.voxel.analyse.history;

import static au.com.webglow.voxel.Constants.GRID_BLOCK_X_COUNT;
import static au.com.webglow.voxel.Constants.GRID_BLOCK_Y_COUNT;
import static au.com.webglow.voxel.Constants.GRID_BLOCK_Z_COUNT;

import au.com.webglow.voxel.analyse.units.FloorGrid;
import au.com.webglow.voxel.analyse.units.GridBlock;

public class FloorGridManager {
	private FloorGrid floorGrid = FloorGrid.newInstance();
	
	public void findOccupiedFloorRegions(GridBlock[][][] gridBlocks) {
		for (int x = 0; x < GRID_BLOCK_X_COUNT; x++) {
			for (int y = 0; y < GRID_BLOCK_Y_COUNT; y++) {
				for (int z = 0; z < GRID_BLOCK_Z_COUNT; z++) {
					floorGrid.processGridBlock(gridBlocks[x][y][z], x, y, z);
				}
			}
		}
	}
	
	public void findForegroundObjects(GridBlock[][][] gridBlocks) {

	}
	
	public void processObjectDetectedGridBlocks(GridBlock[][][] gridBlockPositions) {
		
	}

	public void reset() {
		floorGrid.reset();
	}
}
