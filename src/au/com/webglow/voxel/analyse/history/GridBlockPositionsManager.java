package au.com.webglow.voxel.analyse.history;

import static au.com.webglow.voxel.Constants.DEPTH_COUNT;
import static au.com.webglow.voxel.Constants.GRID_BLOCK_X_COUNT;
import static au.com.webglow.voxel.Constants.GRID_BLOCK_Y_COUNT;
import static au.com.webglow.voxel.Constants.GRID_BLOCK_Z_COUNT;
import static au.com.webglow.voxel.Constants.HIGH_ACCURACY_GRID_BLOCK_X_COUNT;
import static au.com.webglow.voxel.Constants.HIGH_ACCURACY_GRID_BLOCK_Y_COUNT;
import static au.com.webglow.voxel.Constants.HIGH_ACCURACY_GRID_BLOCK_Z_COUNT;
import static au.com.webglow.voxel.Constants.SENSOR_COL_SIZE;
import static au.com.webglow.voxel.Constants.SENSOR_ROW_SIZE;

import au.com.webglow.voxel.analyse.flash.GridBlockCoordinateStorage;
import au.com.webglow.voxel.analyse.units.FramePixelHistory;
import au.com.webglow.voxel.analyse.units.GridBlock;
import au.com.webglow.voxel.analyse.units.GridBlockCoordinate;
import au.com.webglow.voxel.analyse.units.HighAccuracyGridBlock;
import au.com.webglow.voxel.analyse.units.OccupiedFrameDepth;

public class GridBlockPositionsManager {
	private static GridBlock[][][] gridBlocks = new GridBlock[GRID_BLOCK_X_COUNT][GRID_BLOCK_Y_COUNT][GRID_BLOCK_Z_COUNT];
	private static HighAccuracyGridBlock[][][] highAccuracyGridBlocks = new HighAccuracyGridBlock[HIGH_ACCURACY_GRID_BLOCK_X_COUNT][HIGH_ACCURACY_GRID_BLOCK_Y_COUNT][HIGH_ACCURACY_GRID_BLOCK_Z_COUNT];

	public GridBlockPositionsManager() {
		for (int x = 0; x < GRID_BLOCK_X_COUNT; x++) {
			for (int y = 0; y < GRID_BLOCK_Y_COUNT; y++) {
				for (int z = 0; z < GRID_BLOCK_Z_COUNT; z++) {
					gridBlocks[x][y][z] = GridBlock.newInstance(z, x, y);
				}
			}
		}
		for (int highAccuracyBlockX = 0; highAccuracyBlockX < HIGH_ACCURACY_GRID_BLOCK_X_COUNT; highAccuracyBlockX++) {
			for (int highAccuracyBlockY = 0; highAccuracyBlockY < HIGH_ACCURACY_GRID_BLOCK_Y_COUNT; highAccuracyBlockY++) {
				for (int highAccuracyBlockZ = 0; highAccuracyBlockZ < HIGH_ACCURACY_GRID_BLOCK_Z_COUNT; highAccuracyBlockZ++) {
					highAccuracyGridBlocks[highAccuracyBlockX][highAccuracyBlockY][highAccuracyBlockZ] = HighAccuracyGridBlock.newInstance(highAccuracyBlockZ, highAccuracyBlockX, highAccuracyBlockY);
				}
			}
		}
	}
	
	public void findOccupiedGridBlockPositions(FramePixelHistory[][] framePixelHistory) {		
		for (int r = 0; r < (SENSOR_ROW_SIZE-1); r++) {
			for (int c = 0; c < (SENSOR_COL_SIZE-1); c++) {
				OccupiedFrameDepth occupiedFrameDepth = findOccupiedFrameDepth(framePixelHistory,r,c);
				GridBlock gridPosition = findOccupiedGridPosition(r,c,occupiedFrameDepth);
				if (gridPosition != null) {
					gridPosition.flagOccupied();
					gridPosition.setAbleToFormNewlyOccupiedStatus(occupiedFrameDepth.getAbleToFormNewlyOccupiedStatus());
				}
			}
		}
	}
	
	public void processGridBlockOccupiedState() {
		for (int x = 0; x < GRID_BLOCK_X_COUNT; x++) {
			for (int y = 0; y < GRID_BLOCK_Y_COUNT; y++) {
				for (int z = 0; z < GRID_BLOCK_Z_COUNT; z++) {
					gridBlocks[x][y][z].processOccupiedValue();
				}
			}
		}
	}

	/**
	 * Confirmed to be an extremely important function for reducing noise
	 * @param frameDepths
	 * @param r
	 * @param c
	 * @return
	 */
	private OccupiedFrameDepth findOccupiedFrameDepth(FramePixelHistory[][] framePixelHistory, int r, int c) {
		OccupiedFrameDepth retVal = null;
		if (framePixelHistory[r][c].getFrameDepth() < DEPTH_COUNT) {
			if (framePixelHistory[r][c].getFrameDepth() == framePixelHistory[r][c+1].getFrameDepth()
					&& framePixelHistory[r][c].getFrameDepth() == framePixelHistory[r+1][c].getFrameDepth()
					&& framePixelHistory[r][c].getFrameDepth() == framePixelHistory[r+1][c+1].getFrameDepth()) {
				return new OccupiedFrameDepth(framePixelHistory[r][c].getFrameDepth(), framePixelHistory[r][c].getAbleToFormNewlyOccupiedStatus());
			} 
		}
		return retVal;
	}
	
	private GridBlock findOccupiedGridPosition(int r, int c, OccupiedFrameDepth occupiedFrameDepth) {
		GridBlock retVal = null;
		if (occupiedFrameDepth != null) {
			GridBlockCoordinate gridBlockCoordinate = GridBlockCoordinateStorage.getInstance().getPinholeCoordinate(r,c,occupiedFrameDepth.getFrameDepth());
			if (gridBlockCoordinate != null) {
				retVal = gridBlocks[gridBlockCoordinate.getX()][gridBlockCoordinate.getY()][gridBlockCoordinate.getZ()];
			}
		}
		return retVal;
	}
	
//	public void processHighAccuracyBlocks(int[][] highAccuracyFrameDepths) {	
//		//reInitNewMovingObjectHighAccuracyGridBlocks(highAccuracyRegions, newHighAccuracyRegions);
//		//highAccuracyRegions = newHighAccuracyRegions;
//		findOccupiedHighAccuracyGridBlocks(highAccuracyFrameDepths, highAccuracyRegions);
//		processHighAccuracyOccupiedBlocks(highAccuracyRegions);
//	}
	
//	public void findOccupiedHighAccuracyGridBlocks(int[][] highAccuracyFrameDepths) {
//		for (int r = 0; r < (SENSOR_ROW_SIZE-1); r++) {
//			for (int c = 0; c < (SENSOR_COL_SIZE-1); c++) {
//				int pixelDepth = highAccuracyFrameDepths[r][c];
//				if (pixelDepth < HIGH_ACCURACY_DEPTH_COUNT) {
//					HighAccuracyGridBlockCoordinate highAccuracyCoordinate = GridBlockCoordinateStorage.getInstance().getHighAccuracyPinholeCoordinate(r,c,pixelDepth);
//					if (highAccuracyCoordinate != null) {
//						if (parentGridBlock.isNewlyOccupied()) {
//							highAccuracyGridBlocks[highAccuracyCoordinate.getHighAccuracyIndexX()][highAccuracyCoordinate.getHighAccuracyIndexY()][highAccuracyCoordinate.getHighAccuracyIndexZ()].setOccupied(true);
//						}
//					}
//				}
//			}
//		}
//	}
	
//	public void processHighAccuracyBlockOccupiedState() {
//		for (int highAccuracyBlockX = 0; highAccuracyBlockX < HIGH_ACCURACY_GRID_BLOCK_X_COUNT; highAccuracyBlockX++) {
//			for (int highAccuracyBlockY = 0; highAccuracyBlockY < HIGH_ACCURACY_GRID_BLOCK_Y_COUNT; highAccuracyBlockY++) {
//				for (int highAccuracyBlockZ = 0; highAccuracyBlockZ < HIGH_ACCURACY_GRID_BLOCK_Z_COUNT; highAccuracyBlockZ++) {
//					if (parentGridBlock.isNewlyOccupied()) {
//						highAccuracyGridBlocks[highAccuracyBlockX][highAccuracyBlockY][highAccuracyBlockZ].processOccupiedValue();
//					}
//				}
//			}
//		}	
//	}

//	public void findVerticalAveragedPoints(List<HighAccuracyObjectRegion> highAccuracyObjectRegions, CondensedGridBlock[][][] condensedGridBlocks) {
//		if (highAccuracyObjectRegions != null) {
//			for (HighAccuracyObjectRegion highAccuracyObjectRegion: highAccuracyObjectRegions) {
//				for (int y = highAccuracyObjectRegion.getGridBlockRange().getStartY(); y <= highAccuracyObjectRegion.getGridBlockRange().getEndY(); y++) {
//					for (int x = highAccuracyObjectRegion.getGridBlockRange().getStartX(); x <= highAccuracyObjectRegion.getGridBlockRange().getEndX(); x++) {
//						for (int z = highAccuracyObjectRegion.getGridBlockRange().getStartZ(); z <= highAccuracyObjectRegion.getGridBlockRange().getEndZ(); z++) {
//							if (condensedGridBlocks[x][y][z].isHighAccuracyNewlyOccupiedDetected()) {
//								for (int highAccX = condensedGridBlocks[x][y][z].getStartHighAccX(); highAccX <= condensedGridBlocks[x][y][z].getEndHighAccX(); highAccX++) {
//									for (int highAccY = condensedGridBlocks[x][y][z].getStartHighAccY(); highAccY <= condensedGridBlocks[x][y][z].getEndHighAccY(); highAccY++) {
//										for (int highAccZ = condensedGridBlocks[x][y][z].getStartHighAccZ(); highAccZ <= condensedGridBlocks[x][y][z].getEndHighAccZ(); highAccZ++) {
//											if (highAccuracyGridBlocks[highAccX][highAccY][highAccZ].getNewlyOccupiedMonitor().isNewlyOccupied()) {
//												occupiedPointAccumulator.processPoint(highAccX, highAccY, highAccZ);
//											}
//										}	
//									}
//								}
//							}
//							if (condensedGridBlocks[x][y][z].isHighAccuracyNewlyEmptyDetected()) {
//								for (int highAccX = condensedGridBlocks[x][y][z].getStartHighAccX(); highAccX <= condensedGridBlocks[x][y][z].getEndHighAccX(); highAccX++) {
//									for (int highAccY = condensedGridBlocks[x][y][z].getStartHighAccY(); highAccY <= condensedGridBlocks[x][y][z].getEndHighAccY(); highAccY++) {
//										for (int highAccZ = condensedGridBlocks[x][y][z].getStartHighAccZ(); highAccZ <= condensedGridBlocks[x][y][z].getEndHighAccZ(); highAccZ++) {
//											if (highAccuracyGridBlocks[highAccX][highAccY][highAccZ].getNewlyOccupiedMonitor().isNewlyEmpty()) {
//												emptyPointAccumulator.processPoint(highAccX, highAccY, highAccZ);
//											}
//										}	
//									}
//								}
//							}
//						}
//					}
//				}
//				
//				if (occupiedPointAccumulator.isInitialised()) {
//					occupiedPointAccumulator.calculateAveragePoint();
//					Coordinate averagePoint = Coordinate.newInstance(occupiedPointAccumulator.getAvgX(), occupiedPointAccumulator.getAvgY(), occupiedPointAccumulator.getAvgZ());
//					highAccuracyObjectRegion.addOccupiedRegressionPoint(averagePoint);
//				}
//				if (emptyPointAccumulator.isInitialised()) {
//					emptyPointAccumulator.calculateAveragePoint();
//					Coordinate averagePoint = Coordinate.newInstance(emptyPointAccumulator.getAvgX(), emptyPointAccumulator.getAvgY(), emptyPointAccumulator.getAvgZ());
//					highAccuracyObjectRegion.addEmptyRegressionPoint(averagePoint);
//				}
//			}
//		}
//	}
//	
//	private void reInitNewMovingObjectHighAccuracyGridBlocks(List<HighAccuracyRegion> previousHighAccuracyRegions, List<HighAccuracyRegion> newHighAccuracyRegions) {
//		for (HighAccuracyRegion newHighAccuracyRegion: newHighAccuracyRegions) {
//			HighAccuracyRegion overlappingPreviousRegion = findOverlappingRegion(newHighAccuracyRegion, previousHighAccuracyRegions);
//			if (overlappingPreviousRegion != null) {
//				for (int x = newHighAccuracyRegion.getHighAccuracyGridBlockRange().getStartX(); x <= newHighAccuracyRegion.getHighAccuracyGridBlockRange().getEndX(); x++) {
//					for (int y = newHighAccuracyRegion.getHighAccuracyGridBlockRange().getStartY(); y <= newHighAccuracyRegion.getHighAccuracyGridBlockRange().getEndY(); y++) {
//						for (int z = newHighAccuracyRegion.getHighAccuracyGridBlockRange().getStartZ(); z <= newHighAccuracyRegion.getHighAccuracyGridBlockRange().getEndZ(); z++) {
//							if (x < overlappingPreviousRegion.getHighAccuracyGridBlockRange().getStartX()
//								|| x > overlappingPreviousRegion.getHighAccuracyGridBlockRange().getEndX()
//								|| y < overlappingPreviousRegion.getHighAccuracyGridBlockRange().getStartY()
//								|| y > overlappingPreviousRegion.getHighAccuracyGridBlockRange().getEndY()
//								|| z < overlappingPreviousRegion.getHighAccuracyGridBlockRange().getStartZ()
//								|| z > overlappingPreviousRegion.getHighAccuracyGridBlockRange().getEndZ()) {
//								highAccuracyGridBlocks[x][y][z].resetMonitor();
//							} else {
//								highAccuracyGridBlocks[x][y][z].prepareForNextFrame();
//							}
//						}	
//					}	
//				}
//				
//			} else {
//				for (int x = newHighAccuracyRegion.getHighAccuracyGridBlockRange().getStartX(); x <= newHighAccuracyRegion.getHighAccuracyGridBlockRange().getEndX(); x++) {
//					for (int y = newHighAccuracyRegion.getHighAccuracyGridBlockRange().getStartY(); y <= newHighAccuracyRegion.getHighAccuracyGridBlockRange().getEndY(); y++) {
//						for (int z = newHighAccuracyRegion.getHighAccuracyGridBlockRange().getStartZ(); z <= newHighAccuracyRegion.getHighAccuracyGridBlockRange().getEndZ(); z++) {
//							highAccuracyGridBlocks[x][y][z].resetMonitor();
//						}
//					}
//				}
//			}
//		}
//	}
//	
//	private HighAccuracyRegion findOverlappingRegion(HighAccuracyRegion region, List<HighAccuracyRegion> compareRegions) {
//		if (compareRegions == null) {
//			return null;
//		}
//		for (HighAccuracyRegion compareRegion: compareRegions) {
//			if (CalculationUtil.areRegionsOverlapping(region, compareRegion)) {
//				return compareRegion;
//			}
//		}
//		return null;
//	}

	public GridBlock[][][] getGridBlockPositions() {
		return gridBlocks;
	}

	public HighAccuracyGridBlock[][][] getHighAccuracyGridBlocks() {
		return highAccuracyGridBlocks;
	}

	public void reset() {
//		for (int highAccuracyBlockX = 0; highAccuracyBlockX < HIGH_ACCURACY_GRID_BLOCK_X_COUNT; highAccuracyBlockX++) {
//			for (int highAccuracyBlockY = 0; highAccuracyBlockY < HIGH_ACCURACY_GRID_BLOCK_Y_COUNT; highAccuracyBlockY++) {
//				for (int highAccuracyBlockZ = 0; highAccuracyBlockZ < HIGH_ACCURACY_GRID_BLOCK_Z_COUNT; highAccuracyBlockZ++) {
//					highAccuracyGridBlocks[highAccuracyBlockX][highAccuracyBlockY][highAccuracyBlockZ].reset();
//				}
//			}
//		}
		for (int x = 0; x < GRID_BLOCK_X_COUNT; x++) {
			for (int y = 0; y < GRID_BLOCK_Y_COUNT; y++) {
				for (int z = 0; z < GRID_BLOCK_Z_COUNT; z++) {
					gridBlocks[x][y][z].reset();
				}
			}
		}
//		occupiedPointAccumulator.reset();
//		emptyPointAccumulator.reset();
	}	
}
