package au.com.webglow.voxel.analyse.history;

import static au.com.webglow.voxel.Constants.SENSOR_COL_SIZE;
import static au.com.webglow.voxel.Constants.SENSOR_ROW_SIZE;

import au.com.webglow.voxel.analyse.CalculationUtil;
import au.com.webglow.voxel.analyse.units.FramePixelDepths;
import au.com.webglow.voxel.analyse.units.FramePixelHistory;

public class FrameDepthsManager {
	private FramePixelHistory[][] framePixelHistory = new FramePixelHistory[SENSOR_ROW_SIZE][SENSOR_COL_SIZE];
	private int[][] highAccuracyFrameDepths;
	
	public FrameDepthsManager() {
		for (int row = 0; row < SENSOR_ROW_SIZE; row++) {
			for (int col = 0; col < SENSOR_COL_SIZE; col++) {
				framePixelHistory[row][col] = new FramePixelHistory();
			}
		}
	}
	
	public void findDepthsForFrame(byte[] frame) {
		FramePixelDepths framePixelDepths = CalculationUtil.calcBlockFrameDepths(frame, framePixelHistory);
		setHighAccuracyFrameDepths(framePixelDepths.getHighAccuracyFrameDepths());
		for (int row = 0; row < SENSOR_ROW_SIZE; row++) {
			for (int col = 0; col < SENSOR_COL_SIZE; col++) {
				framePixelHistory[row][col].processFrameDepth(framePixelDepths.getFrameDepths()[row][col], framePixelDepths.getFrameDepthChanges()[row][col]);
			}
		}
	}
	
	public void reset() {
		for (int row = 0; row < SENSOR_ROW_SIZE; row++) {
			for (int col = 0; col < SENSOR_COL_SIZE; col++) {
				framePixelHistory[row][col].prepareForNextFrame();
			}
		}
	}

	public FramePixelHistory[][] getFramePixelHistory() {
		return framePixelHistory;
	}

	public void setFramePixelHistory(FramePixelHistory[][] framePixelHistory) {
		this.framePixelHistory = framePixelHistory;
	}

	public int[][] getHighAccuracyFrameDepths() {
		return highAccuracyFrameDepths;
	}

	public void setHighAccuracyFrameDepths(int[][] highAccuracyFrameDepths) {
		this.highAccuracyFrameDepths = highAccuracyFrameDepths;
	}
}
