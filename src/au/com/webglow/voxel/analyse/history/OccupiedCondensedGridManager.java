package au.com.webglow.voxel.analyse.history;

import static au.com.webglow.voxel.Constants.FLOOR_GRID_CELL_COL_COUNT;
import static au.com.webglow.voxel.Constants.FLOOR_GRID_CELL_DEPTH_COUNT;
import static au.com.webglow.voxel.Constants.GRID_BLOCK_X_COUNT;
import static au.com.webglow.voxel.Constants.GRID_BLOCK_Y_COUNT;
import static au.com.webglow.voxel.Constants.GRID_BLOCK_Z_COUNT;

import au.com.webglow.voxel.analyse.units.CondensedGridBlock;
import au.com.webglow.voxel.analyse.units.GridBlock;
import au.com.webglow.voxel.analyse.units.MinMaxValues;

public class OccupiedCondensedGridManager {
	private static final int MOVEMENT_DETECTED_NEWLY_OCCUPIED_THRESHOLD = 3;
	private static int[][] condensedColumns = new int[FLOOR_GRID_CELL_COL_COUNT][FLOOR_GRID_CELL_DEPTH_COUNT];
	private static CondensedGridBlock[][][] condensedGridBlocks = new CondensedGridBlock[GRID_BLOCK_X_COUNT][GRID_BLOCK_Y_COUNT][GRID_BLOCK_Z_COUNT];

	public OccupiedCondensedGridManager() {	
		for (int x = 0; x < FLOOR_GRID_CELL_COL_COUNT; x++) {
			for (int z = 0; z < FLOOR_GRID_CELL_DEPTH_COUNT; z++) {
				condensedColumns[x][z] = 0;
			}
		}
		for (int x = 0; x < GRID_BLOCK_X_COUNT; x++) {
			for (int y = 0; y < GRID_BLOCK_Y_COUNT; y++) {
				for (int z = 0; z < GRID_BLOCK_Z_COUNT; z++) {
					condensedGridBlocks[x][y][z] = CondensedGridBlock.newInstance(x, y, z);
				}
			}
		}
	}

	public void processGridPositions(GridBlock[][][] gridBlocks) {
		for (int x = 0; x < GRID_BLOCK_X_COUNT; x++) {
			for (int y = 0; y < GRID_BLOCK_Y_COUNT; y++) {
				for (int z = 0; z < GRID_BLOCK_Z_COUNT; z++) {
					if (gridBlocks[x][y][z].getOccupiedMonitor().isNewlyOccupied()) {
						condensedColumns[x][z]++;
					}
				}
			}
		}
		
		for (int x = 0; x < GRID_BLOCK_X_COUNT; x++) {
			for (int z = 0; z < GRID_BLOCK_Z_COUNT; z++) {
				if (condensedColumns[x][z] >= MOVEMENT_DETECTED_NEWLY_OCCUPIED_THRESHOLD) {	
					for (int y = 0; y < GRID_BLOCK_Y_COUNT; y++) {
						MinMaxValues minMaxValues = determineMinMaxOccupiedHeight(x, z, gridBlocks);
						for (int flagY = minMaxValues.getMinValue(); flagY <= minMaxValues.getMaxValue(); flagY++) {
							if (flagY >= 0) {
								gridBlocks[x][flagY][z].getOccupiedMonitor().flagObjectDetected();	
							}
						}
					}
				}
			}
		}
	}
		
	private MinMaxValues determineMinMaxOccupiedHeight(int x, int z, GridBlock[][][] gridBlocks) {
		MinMaxValues retVal = new MinMaxValues();
		for (int y = 0; y < GRID_BLOCK_Y_COUNT; y++) {
			if (gridBlocks[x][y][z].getOccupiedMonitor().isNewlyOccupied() || 
					gridBlocks[x][y][z].getOccupiedMonitor().isObjectDetected()) {
				retVal.processValue(y);
			}
		}
		return retVal;
	}

	public CondensedGridBlock[][][] getCondensedGridBlocks() {
		return condensedGridBlocks;
	}
	
	public void reset() {
		for (int x = 0; x < FLOOR_GRID_CELL_COL_COUNT; x++) {
			for (int z = 0; z < FLOOR_GRID_CELL_DEPTH_COUNT; z++) {
				condensedColumns[x][z] = 0;
			}
		}
		for (int x = 0; x < GRID_BLOCK_X_COUNT; x++) {
			for (int y = 0; y < GRID_BLOCK_Y_COUNT; y++) {
				for (int z = 0; z < GRID_BLOCK_Z_COUNT; z++) {
					condensedGridBlocks[x][y][z].reset();
				}
			}
		}
	}
}
