package au.com.webglow.voxel.analyse.units;

public class GridBlockPosition {
	private short z; 
	private short x; 
	private short y;
	
	public static GridBlockPosition newInstance(short z, short x, short y) {
		GridBlockPosition retVal = new GridBlockPosition();
		retVal.setZ(z);
		retVal.setX(x);
		retVal.setY(y);
		return retVal;
	}
	
	private GridBlockPosition() {}

	public short getZ() {
		return z;
	}

	public void setZ(short z) {
		this.z = z;
	}

	public short getX() {
		return x;
	}

	public void setX(short x) {
		this.x = x;
	}

	public short getY() {
		return y;
	}

	public void setY(short y) {
		this.y = y;
	}
}
