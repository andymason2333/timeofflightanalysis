package au.com.webglow.voxel.analyse.units;

public class HighAccuracyGridBlockCoordinate {
	private int blockZ;
	private int blockX;
	private int blockY;
	private int highAccuracyIndexZ;
	private int highAccuracyIndexX;
	private int highAccuracyIndexY;

	public static HighAccuracyGridBlockCoordinate newInstance(int blockZ, int blockX, int blockY, int highAccuracyIndexZ, int highAccuracyIndexX, int highAccuracyIndexY) {
		return new HighAccuracyGridBlockCoordinate(blockZ, blockX, blockY, highAccuracyIndexZ, highAccuracyIndexX, highAccuracyIndexY);
	}

	private HighAccuracyGridBlockCoordinate(int blockZ, int blockX, int blockY, int highAccuracyIndexZ, int highAccuracyIndexX, int highAccuracyIndexY) {
		this.blockZ = blockZ;
		this.blockX = blockX;
		this.blockY = blockY;
		this.highAccuracyIndexZ = highAccuracyIndexZ;
		this.highAccuracyIndexX = highAccuracyIndexX;
		this.highAccuracyIndexY = highAccuracyIndexY;
	}

	public int getBlockZ() {
		return blockZ;
	}

	public int getBlockX() {
		return blockX;
	}

	public int getBlockY() {
		return blockY;
	}

	public int getHighAccuracyIndexZ() {
		return highAccuracyIndexZ;
	}

	public int getHighAccuracyIndexX() {
		return highAccuracyIndexX;
	}

	public int getHighAccuracyIndexY() {
		return highAccuracyIndexY;
	}
}
