package au.com.webglow.voxel.analyse.units;

public class PixelCoordinate {
	private Integer r;
	private Integer c; 

	public static PixelCoordinate newInstance(int r, int c) {
		return new PixelCoordinate(r, c);
	}

	private PixelCoordinate(int r, int c) {
		this.r = r;
		this.c = c;
	}

	public Integer getR() {
		return r;
	}

	public void setR(Integer r) {
		this.r = r;
	}

	public Integer getC() {
		return c;
	}

	public void setC(Integer c) {
		this.c = c;
	}
}
