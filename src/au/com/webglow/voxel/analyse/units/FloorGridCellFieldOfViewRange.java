package au.com.webglow.voxel.analyse.units;

import static au.com.webglow.voxel.Constants.GRID_BLOCK_X_COUNT;

import au.com.webglow.voxel.analyse.CalculationUtil;

public class FloorGridCellFieldOfViewRange {
	private float minGradient;
	private float maxGradient;
	private int distanceToCamera;

	public static FloorGridCellFieldOfViewRange newInstance(int x, int z) {		
		FloorGridCellFieldOfViewRange retVal = new FloorGridCellFieldOfViewRange();
		int calcZ = Math.max(0, z);
		int calcXMin = Math.max(0, x-1);
		int calcXMax = Math.min((GRID_BLOCK_X_COUNT-1), x+1);
		float gradient1 = CalculationUtil.calculateFloorGridGradient(calcXMin, calcZ);
		float gradient2 = CalculationUtil.calculateFloorGridGradient(calcXMax, calcZ);
		int distanceToCamera = CalculationUtil.calculateFloorGridDistanceToCamera(calcXMin, calcZ);
		
		retVal.setMinGradient(Math.min(gradient1, gradient2));
		retVal.setMaxGradient(Math.max(gradient1, gradient2));
		retVal.setDistanceToCamera(distanceToCamera);
		return retVal;
	}

	public float getMinGradient() {
		return minGradient;
	}

	public void setMinGradient(float minGradient) {
		this.minGradient = minGradient;
	}

	public float getMaxGradient() {
		return maxGradient;
	}

	public void setMaxGradient(float maxGradient) {
		this.maxGradient = maxGradient;
	}

	public int getDistanceToCamera() {
		return distanceToCamera;
	}

	public void setDistanceToCamera(int distanceToCamera) {
		this.distanceToCamera = distanceToCamera;
	}
}
