package au.com.webglow.voxel.analyse.units;

import static au.com.webglow.voxel.Constants.GRID_BLOCK_X_COUNT;
import static au.com.webglow.voxel.Constants.GRID_BLOCK_Y_COUNT;
import static au.com.webglow.voxel.Constants.GRID_BLOCK_Z_COUNT;

import au.com.webglow.voxel.analyse.CalculationUtil;
import au.com.webglow.voxel.analyse.flash.GridBlockCoordinateStorage;
import au.com.webglow.voxel.analyse.helper.AbleToFormNewlyOccupiedStatus;
import au.com.webglow.voxel.analyse.helper.GridBlockNewlyOccupiedMonitor;

public class GridBlock implements GridBlockPipeBuffer {
	private int hashCode;
	private int blockZ; 
	private int blockX;
	private int blockY;
	private GridBlockPosition gridBlockPosition;
	private boolean occupied = false;
	private AbleToFormNewlyOccupiedStatus ableToFormNewlyOccupiedStatus = null;
	private GridBlockNewlyOccupiedMonitor occupiedMonitor = new GridBlockNewlyOccupiedMonitor();
	
	public static GridBlock newInstance(int blockZ, int blockX, int blockY) {
		GridBlockPosition gridBlockPosition = GridBlockCoordinateStorage.getInstance().getGridBlockPosition(blockX, blockY, blockZ);
		GridBlock retVal = new GridBlock();
		retVal.setGridBlockPosition(gridBlockPosition);
		retVal.setBlockZ(blockZ);
		retVal.setBlockX(blockX);
		retVal.setBlockY(blockY);
		return retVal;
	}

	private GridBlock() {
		hashCode = (blockZ * (GRID_BLOCK_Z_COUNT+GRID_BLOCK_X_COUNT+GRID_BLOCK_Y_COUNT)) 
	    		+ (blockX * (GRID_BLOCK_X_COUNT+GRID_BLOCK_Y_COUNT))
				+ (blockY * GRID_BLOCK_Y_COUNT);
	}
	
	public void flagOccupied() {
		setOccupied(true);
	}
	
	public void processOccupiedValue() {
		occupiedMonitor.processOccupiedValue(isOccupied(), ableToFormNewlyOccupiedStatus);
	}
	
	public boolean isNewlyOccupied() {
		return occupiedMonitor.isNewlyOccupied();
	}
	
	public void reset() {
		occupied = false;
		ableToFormNewlyOccupiedStatus = null;
		occupiedMonitor.prepareForNextFrame();
	}
	
	public int getBlockZ() {
		return blockZ;
	}

	public void setBlockZ(int blockZ) {
		this.blockZ = blockZ;
	}

	public int getBlockX() {
		return blockX;
	}

	public void setBlockX(int blockX) {
		this.blockX = blockX;
	}

	public int getBlockY() {
		return blockY;
	}

	public void setBlockY(int blockY) {
		this.blockY = blockY;
	}

	public GridBlockPosition getGridBlockPosition() {
		return gridBlockPosition;
	}

	public void setGridBlockPosition(GridBlockPosition gridBlockPosition) {
		this.gridBlockPosition = gridBlockPosition;
	}

	public boolean isOccupied() {
		return occupied;
	}

	public void setOccupied(boolean occupied) {
		this.occupied = occupied;
	}

	public AbleToFormNewlyOccupiedStatus getAbleToFormNewlyOccupiedStatus() {
		return ableToFormNewlyOccupiedStatus;
	}

	public void setAbleToFormNewlyOccupiedStatus(AbleToFormNewlyOccupiedStatus ableToFormNewlyOccupiedStatus) {
		this.ableToFormNewlyOccupiedStatus = ableToFormNewlyOccupiedStatus;
	}

	public GridBlockNewlyOccupiedMonitor getOccupiedMonitor() {
		return occupiedMonitor;
	}

	public void setOccupiedMonitor(GridBlockNewlyOccupiedMonitor occupiedMonitor) {
		this.occupiedMonitor = occupiedMonitor;
	}

	@Override
	public byte[] getPipeBuffer() {
		return CalculationUtil.calcGridBlockPositionPipeBuffer(gridBlockPosition);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (!(o instanceof GridBlock))
			return false;

		GridBlock other = (GridBlock) o;
		if (this.blockZ != other.blockZ)
			return false;
		if (this.blockX != other.blockX)
			return false;
		if (this.blockY != other.blockY)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return hashCode;
	}
}
