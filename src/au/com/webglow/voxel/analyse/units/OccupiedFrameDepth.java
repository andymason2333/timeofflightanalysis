package au.com.webglow.voxel.analyse.units;

import au.com.webglow.voxel.analyse.helper.AbleToFormNewlyOccupiedStatus;

public class OccupiedFrameDepth {
	private int frameDepth;
	private AbleToFormNewlyOccupiedStatus ableToFormNewlyOccupiedStatus;
	
	public OccupiedFrameDepth(int frameDepth, AbleToFormNewlyOccupiedStatus ableToFormNewlyOccupiedStatus) {
		this.frameDepth = frameDepth;
		this.ableToFormNewlyOccupiedStatus = ableToFormNewlyOccupiedStatus;
	}
	
	public int getFrameDepth() {
		return frameDepth;
	}

	public void setFrameDepth(int frameDepth) {
		this.frameDepth = frameDepth;
	}

	public AbleToFormNewlyOccupiedStatus getAbleToFormNewlyOccupiedStatus() {
		return ableToFormNewlyOccupiedStatus;
	}

	public void setAbleToFormNewlyOccupiedStatus(AbleToFormNewlyOccupiedStatus ableToFormNewlyOccupiedStatus) {
		this.ableToFormNewlyOccupiedStatus = ableToFormNewlyOccupiedStatus;
	}
}
