package au.com.webglow.voxel.analyse.units;

import au.com.webglow.voxel.Constants;

public class FloorGridRegion extends FlatRegion {
	private int minDistanceToCamera = -1;
	private float minGradient = -1;
	private float maxGradient = -1;
	private int cellCount;
	private int minY;
	private int maxY;

	public static FloorGridRegion newInstance(FlatRegion flatRegion) {
		FloorGridRegion retVal = new FloorGridRegion(flatRegion);
		retVal.setCellCount(flatRegion.getRegionCoordinates().size());
		retVal.setMinY(0);
		retVal.setMaxY(Constants.GRID_BLOCK_Y_COUNT);
		return retVal;
	}

	private FloorGridRegion(FlatRegion flatRegion) {
		super(flatRegion);
	}
	
	public void addRegionCoordinate(FlatCellCoordinate coordinate) {
		super.addRegionCoordinate(coordinate);
	}
	
	public int getMinY() {
		return minY;
	}

	public void setMinY(int minY) {
		this.minY = minY;
	}

	public int getMaxY() {
		return maxY;
	}

	public void setMaxY(int maxY) {
		this.maxY = maxY;
	}

	public int getMinDistanceToCamera() {
		return minDistanceToCamera;
	}

	public void setMinDistanceToCamera(int minDistanceToCamera) {
		this.minDistanceToCamera = minDistanceToCamera;
	}

	public float getMinGradient() {
		return minGradient;
	}

	public void setMinGradient(float minGradient) {
		this.minGradient = minGradient;
	}

	public float getMaxGradient() {
		return maxGradient;
	}

	public void setMaxGradient(float maxGradient) {
		this.maxGradient = maxGradient;
	}

	public int getCellCount() {
		return cellCount;
	}

	public void setCellCount(int cellCount) {
		this.cellCount = cellCount;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (!(o instanceof FloorGridRegion))
			return false;

		FloorGridRegion other = (FloorGridRegion) o;
		if (this.getMinX() != other.getMinX())
			return false;
		if (this.getMaxX() != other.getMaxX())
			return false;
		if (this.getMinZ() != other.getMinZ())
			return false;
		if (this.getMaxZ() != other.getMaxZ())
			return false;
		if (this.getCellCount() != other.getCellCount())
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return (getMinX()+getMaxX())*(getMinZ()+getMaxZ());
	}
	
	@Override
	public String toString() {
		return String.format("minX=%d, maxX=%d, minZ=%d, maxZ=%d", 
				getMinX(), getMaxX(), getMinZ(), getMaxZ());
	}
}
