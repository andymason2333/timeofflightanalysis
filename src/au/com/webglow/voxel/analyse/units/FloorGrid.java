package au.com.webglow.voxel.analyse.units;

import static au.com.webglow.voxel.Constants.FLOOR_GRID_CELL_COL_COUNT;
import static au.com.webglow.voxel.Constants.FLOOR_GRID_CELL_DEPTH_COUNT;

import au.com.webglow.voxel.analyse.helper.FloorGridRegionOccupiedMonitor;

public class FloorGrid {
	private FloorGridCell[][] cells = new FloorGridCell[FLOOR_GRID_CELL_COL_COUNT][FLOOR_GRID_CELL_DEPTH_COUNT];
	private FloorGridRegionOccupiedMonitor[][] occupiedMonitorRegions = new FloorGridRegionOccupiedMonitor[FLOOR_GRID_CELL_COL_COUNT-1][FLOOR_GRID_CELL_DEPTH_COUNT-1];
	
	public static FloorGrid newInstance() {
		return new FloorGrid();
	}
	
	private FloorGrid() {
		for (int x = 0; x < FLOOR_GRID_CELL_COL_COUNT; x++) {
			for (int z = 0; z < FLOOR_GRID_CELL_DEPTH_COUNT; z++) {
				cells[x][z] = FloorGridCell.newInstance(x, z);
			}
		}
		for (int x = 0; x < FLOOR_GRID_CELL_COL_COUNT-1; x++) {
			for (int z = 0; z < FLOOR_GRID_CELL_DEPTH_COUNT-1; z++) {
				occupiedMonitorRegions[x][z] = FloorGridRegionOccupiedMonitor.newInstance();
			}
		}
	}

	public void processGridBlock(GridBlock gridBlock, int x, int y, int z) {
		if (gridBlock.isNewlyOccupied()) {		
			if (x < (FLOOR_GRID_CELL_COL_COUNT-1) && z < (FLOOR_GRID_CELL_DEPTH_COUNT-1)) {
				occupiedMonitorRegions[x][z].processNewlyOccupiedGridBlock(x,y,z);
			}
			if (x > 0 && z < (FLOOR_GRID_CELL_DEPTH_COUNT-1)) {
				occupiedMonitorRegions[x-1][z].processNewlyOccupiedGridBlock(x,y,z);
			}
			if (x < (FLOOR_GRID_CELL_COL_COUNT-1) && z > 0) {
				occupiedMonitorRegions[x][z-1].processNewlyOccupiedGridBlock(x,y,z);
			}
			if (x > 0 && z > 0) {
				occupiedMonitorRegions[x-1][z-1].processNewlyOccupiedGridBlock(x,y,z);
			}
		}
	}

	public void reset() {
		for (int x = 0; x < FLOOR_GRID_CELL_COL_COUNT; x++) {
			for (int z = 0; z < FLOOR_GRID_CELL_DEPTH_COUNT; z++) {
				cells[x][z].reset();
			}
		}
		for (int x = 0; x < FLOOR_GRID_CELL_COL_COUNT-1; x++) {
			for (int z = 0; z < FLOOR_GRID_CELL_DEPTH_COUNT-1; z++) {
				occupiedMonitorRegions[x][z].reset();
			}
		}
	}
}
