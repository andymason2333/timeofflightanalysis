package au.com.webglow.voxel.analyse.units;

public class MinMaxValues {
	private int minValue = -1;
	private int maxValue = -1;
	
	public void processValue(int value) {
		if (minValue == -1 || value < minValue) {
			minValue = value;
		}
		if (maxValue == -1 || value > maxValue) {
			maxValue = value;
		}
	}
	
	public boolean isInitialised() {
		return minValue != -1 && maxValue != -1;
	}
	
	public int getMinValue() {
		return minValue;
	}
	
	public void setMinValue(int minValue) {
		this.minValue = minValue;
	}
	
	public int getMaxValue() {
		return maxValue;
	}
	
	public void setMaxValue(int maxValue) {
		this.maxValue = maxValue;
	}
	
	public MinMaxValues clone() {
		MinMaxValues clone = new MinMaxValues();
		clone.setMinValue(minValue);
		clone.setMaxValue(maxValue);
		return clone;
	}

	public void reset() {
		minValue = -1;
		maxValue = -1;
	}
}
