package au.com.webglow.voxel.analyse.units;

import au.com.webglow.voxel.Constants;

public class CondensedGridBlock {
	private int startHighAccX;
	private int endHighAccX;
	private int startHighAccY;
	private int endHighAccY;
	private int startHighAccZ;
	private int endHighAccZ;
	private int highAccuracyNewlyOccupiedCount = 0;
	private int highAccuracyNewlyEmptyCount = 0;
	private boolean highAccuracyNewlyOccupiedDetected = false;
	private boolean highAccuracyNewlyEmptyDetected = false;
	
	public static CondensedGridBlock newInstance(int x, int y, int z) {
		int startHighAccX = x * Constants.HIGH_ACCURACY_PER_GRID_BLOCK;
		int endHighAccX = (startHighAccX + Constants.HIGH_ACCURACY_PER_GRID_BLOCK) - 1;
		int startHighAccY = y * Constants.HIGH_ACCURACY_PER_GRID_BLOCK;
		int endHighAccY = (startHighAccY + Constants.HIGH_ACCURACY_PER_GRID_BLOCK) - 1;
		int startHighAccZ = z * Constants.HIGH_ACCURACY_PER_GRID_BLOCK;
		int endHighAccZ = (startHighAccZ + Constants.HIGH_ACCURACY_PER_GRID_BLOCK) - 1;
		
		CondensedGridBlock retVal = new CondensedGridBlock();
		retVal.setStartHighAccX(startHighAccX);
		retVal.setEndHighAccX(endHighAccX);
		retVal.setStartHighAccY(startHighAccY);
		retVal.setEndHighAccY(endHighAccY);
		retVal.setStartHighAccZ(startHighAccZ);
		retVal.setEndHighAccZ(endHighAccZ);
		return retVal;
	}

	private CondensedGridBlock() {}
	
	public void incrementHighAccuracyNewlyOccupiedCount() {
		highAccuracyNewlyOccupiedCount++;
	}
	
	public void incrementHighAccuracyNewlyEmptyCount() {
		highAccuracyNewlyEmptyCount++;
	}
	
	public void flagHighAccuracyNewlyOccupiedDetected() {
		highAccuracyNewlyOccupiedDetected = true;
	}
	
	public void flagHighAccuracyNewlyEmptyDetected() {
		highAccuracyNewlyEmptyDetected = true;
	}
	
	public void reset() {
		highAccuracyNewlyOccupiedCount = 0;
		highAccuracyNewlyEmptyCount = 0;
		highAccuracyNewlyOccupiedDetected = false;
		highAccuracyNewlyEmptyDetected = false;
	}

	public int getStartHighAccX() {
		return startHighAccX;
	}

	public void setStartHighAccX(int startHighAccX) {
		this.startHighAccX = startHighAccX;
	}

	public int getEndHighAccX() {
		return endHighAccX;
	}

	public void setEndHighAccX(int endHighAccX) {
		this.endHighAccX = endHighAccX;
	}

	public int getStartHighAccY() {
		return startHighAccY;
	}

	public void setStartHighAccY(int startHighAccY) {
		this.startHighAccY = startHighAccY;
	}

	public int getEndHighAccY() {
		return endHighAccY;
	}

	public void setEndHighAccY(int endHighAccY) {
		this.endHighAccY = endHighAccY;
	}

	public int getStartHighAccZ() {
		return startHighAccZ;
	}

	public void setStartHighAccZ(int startHighAccZ) {
		this.startHighAccZ = startHighAccZ;
	}

	public int getEndHighAccZ() {
		return endHighAccZ;
	}

	public void setEndHighAccZ(int endHighAccZ) {
		this.endHighAccZ = endHighAccZ;
	}

	public int getHighAccuracyNewlyOccupiedCount() {
		return highAccuracyNewlyOccupiedCount;
	}

	public void setHighAccuracyNewlyOccupiedCount(int highAccuracyNewlyOccupiedCount) {
		this.highAccuracyNewlyOccupiedCount = highAccuracyNewlyOccupiedCount;
	}

	public int getHighAccuracyNewlyEmptyCount() {
		return highAccuracyNewlyEmptyCount;
	}

	public void setHighAccuracyNewlyEmptyCount(int highAccuracyNewlyEmptyCount) {
		this.highAccuracyNewlyEmptyCount = highAccuracyNewlyEmptyCount;
	}

	public boolean isHighAccuracyNewlyOccupiedDetected() {
		return highAccuracyNewlyOccupiedDetected;
	}

	public void setHighAccuracyNewlyOccupiedDetected(boolean highAccuracyNewlyOccupiedDetected) {
		this.highAccuracyNewlyOccupiedDetected = highAccuracyNewlyOccupiedDetected;
	}

	public boolean isHighAccuracyNewlyEmptyDetected() {
		return highAccuracyNewlyEmptyDetected;
	}

	public void setHighAccuracyNewlyEmptyDetected(boolean highAccuracyNewlyEmptyDetected) {
		this.highAccuracyNewlyEmptyDetected = highAccuracyNewlyEmptyDetected;
	}
}
