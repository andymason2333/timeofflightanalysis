package au.com.webglow.voxel.analyse.units;

public class Coordinate {
	private int x; 
	private int y; 
	private int z;
	
	public static Coordinate newInstance(int x, int y, int z) {
		return new Coordinate(x, y, z);
	}
	
	public Coordinate(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public int getX() {
		return x;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public int getZ() {
		return z;
	}
	
	public void setZ(int z) {
		this.z = z;
	}
	
	@Override
	public String toString() {
		return String.format("x=%d y=%d z=%d", x, y, z);
	}
}
