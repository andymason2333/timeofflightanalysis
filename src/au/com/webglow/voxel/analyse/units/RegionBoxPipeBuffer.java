package au.com.webglow.voxel.analyse.units;

public interface RegionBoxPipeBuffer {
	byte[] getPipeBuffer();
}
