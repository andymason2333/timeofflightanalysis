package au.com.webglow.voxel.analyse.units;

public class RegressionPointAccumulator {
	private int sumX = 0;
	private int sumY = 0;
	private int sumZ = 0;
	private int pointCount = 0;
	private int avgX = 0;
	private int avgY = 0;
	private int avgZ = 0;
	
	public void processPoint(int x, int y, int z) {
		sumX += x;
		sumY += y;
		sumZ += z;
		pointCount ++;
	}
	
	public void calculateAveragePoint() {
		avgX = sumX / pointCount;
		avgY = sumY / pointCount;
		avgZ = sumZ / pointCount;
	}
	
	public boolean isInitialised() {
		return pointCount > 0;
	}

	public int getAvgX() {
		return avgX;
	}

	public int getAvgY() {
		return avgY;
	}

	public int getAvgZ() {
		return avgZ;
	}

	public void reset() {
		sumX = 0;
		sumY = 0;
		sumZ = 0;
		pointCount = 0;
		avgX = 0;
		avgY = 0;
		avgZ = 0;
	}
}
