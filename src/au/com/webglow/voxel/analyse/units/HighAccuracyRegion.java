package au.com.webglow.voxel.analyse.units;

import java.util.ArrayList;
import java.util.List;

import au.com.webglow.voxel.Constants;

public class HighAccuracyRegion {
	public static final int EXTRA_GRID_BLOCK_PADDING = 1;
	
	private GridBlockRange gridBlockRange;
	private GridBlockRange highAccuracyGridBlockRange;
	private HighAccuracyMovementRegion highAccuracyNewlyOccupiedRegion = null;
	private HighAccuracyMovementRegion highAccuracyNewlyEmptyRegion = null;
	
	public static HighAccuracyRegion newInstance(FloorGridRegion floorGridRegion) {
		HighAccuracyRegion retVal = new HighAccuracyRegion();
		retVal.setGridBlockRange(GridBlockRange.newInstance(
				Math.max(0, floorGridRegion.getMinX()-EXTRA_GRID_BLOCK_PADDING), 
				Math.max(0, floorGridRegion.getMinY()-EXTRA_GRID_BLOCK_PADDING), 
				Math.max(0, floorGridRegion.getMinZ()-EXTRA_GRID_BLOCK_PADDING), 
				Math.min(Constants.GRID_BLOCK_X_COUNT-1, floorGridRegion.getMaxX()+EXTRA_GRID_BLOCK_PADDING), 
				Math.min(Constants.GRID_BLOCK_Y_COUNT-1, floorGridRegion.getMaxY()+EXTRA_GRID_BLOCK_PADDING), 
				Math.min(Constants.GRID_BLOCK_Z_COUNT-1, floorGridRegion.getMaxZ()+EXTRA_GRID_BLOCK_PADDING)));
		
		int startX = retVal.getGridBlockRange().getStartX() * Constants.HIGH_ACCURACY_PER_GRID_BLOCK;
		int startY = retVal.getGridBlockRange().getStartY() * Constants.HIGH_ACCURACY_PER_GRID_BLOCK;
		int startZ = retVal.getGridBlockRange().getStartZ() * Constants.HIGH_ACCURACY_PER_GRID_BLOCK;
		int endX = startX + (((retVal.getGridBlockRange().getEndX() - retVal.getGridBlockRange().getStartX()) + 1) * Constants.HIGH_ACCURACY_PER_GRID_BLOCK) - 1;
		int endY = startY + (((retVal.getGridBlockRange().getEndY() - retVal.getGridBlockRange().getStartY()) + 1) * Constants.HIGH_ACCURACY_PER_GRID_BLOCK) - 1;
		int endZ = startZ + (((retVal.getGridBlockRange().getEndZ() - retVal.getGridBlockRange().getStartZ()) + 1) * Constants.HIGH_ACCURACY_PER_GRID_BLOCK) - 1;
		retVal.setHighAccuracyGridBlockRange(GridBlockRange.newInstance(startX, startY, startZ, endX, endY, endZ));
		return retVal;
	}
	
	public static List<HighAccuracyRegion> newListInstance(List<FloorGridRegion> floorGridRegions) {
		List<HighAccuracyRegion> retVal = new ArrayList<HighAccuracyRegion>(floorGridRegions.size());
		for (FloorGridRegion floorGridRegion: floorGridRegions) {
			HighAccuracyRegion gridBlockHighAccuracyRegion = newInstance(floorGridRegion);
			retVal.add(gridBlockHighAccuracyRegion);
		}
		return retVal;
	}

	private HighAccuracyRegion() {}
	
	public GridBlockRange getGridBlockRange() {
		return gridBlockRange;
	}

	public void setGridBlockRange(GridBlockRange gridBlockRange) {
		this.gridBlockRange = gridBlockRange;
	}

	public GridBlockRange getHighAccuracyGridBlockRange() {
		return highAccuracyGridBlockRange;
	}

	public void setHighAccuracyGridBlockRange(GridBlockRange highAccuracyGridBlockRange) {
		this.highAccuracyGridBlockRange = highAccuracyGridBlockRange;
	}
	
	public HighAccuracyMovementRegion getHighAccuracyNewlyOccupiedRegion() {
		return highAccuracyNewlyOccupiedRegion;
	}

	public void setHighAccuracyNewlyOccupiedRegion(HighAccuracyMovementRegion highAccuracyNewlyOccupiedRegion) {
		this.highAccuracyNewlyOccupiedRegion = highAccuracyNewlyOccupiedRegion;
	}

	public HighAccuracyMovementRegion getHighAccuracyNewlyEmptyRegion() {
		return highAccuracyNewlyEmptyRegion;
	}

	public void setHighAccuracyNewlyEmptyRegion(HighAccuracyMovementRegion highAccuracyNewlyEmptyRegion) {
		this.highAccuracyNewlyEmptyRegion = highAccuracyNewlyEmptyRegion;
	}
}
