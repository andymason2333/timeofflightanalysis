package au.com.webglow.voxel.analyse.units;

public interface LinePipeBuffer {
	byte[] getPipeBuffer();
}
