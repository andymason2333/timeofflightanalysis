package au.com.webglow.voxel.analyse.units;

public class Region {
	private int minX;
	private int maxX;
	private int minZ;
	private int maxZ;
	private int minY;
	private int maxY;
	
	public static Region newInstance(int minX, int maxX, int minZ, int maxZ, int minY, int maxY) {
		return new Region(minX, maxX, minZ, maxZ, minY, maxY);
	}
	
	protected Region(int minX, int maxX, int minZ, int maxZ, int minY, int maxY) {
		this.minX = minX;
		this.maxX = maxX;
		this.minZ = minZ;
		this.maxZ = maxZ;
		this.minY = minY;
		this.maxY = maxY;
	}

	public int getMinX() {
		return minX;
	}

	public void setMinX(int minX) {
		this.minX = minX;
	}

	public int getMaxX() {
		return maxX;
	}

	public void setMaxX(int maxX) {
		this.maxX = maxX;
	}

	public int getMinZ() {
		return minZ;
	}

	public void setMinZ(int minZ) {
		this.minZ = minZ;
	}

	public int getMaxZ() {
		return maxZ;
	}

	public void setMaxZ(int maxZ) {
		this.maxZ = maxZ;
	}

	public int getMinY() {
		return minY;
	}

	public void setMinY(int minY) {
		this.minY = minY;
	}

	public int getMaxY() {
		return maxY;
	}

	public void setMaxY(int maxY) {
		this.maxY = maxY;
	}
}
