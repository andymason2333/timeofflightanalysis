package au.com.webglow.voxel.analyse.units;

import static au.com.webglow.voxel.Constants.GRID_BLOCK_X_COUNT;

public class FlatCellCoordinate {
	private Integer x;
	private Integer z; 

	public static FlatCellCoordinate newInstance(int x, int z) {
		return new FlatCellCoordinate(x, z);
	}

	private FlatCellCoordinate(int x, int z) {
		this.x = x;
		this.z = z;
	}
	
	public Integer getSortNumber() {
		return (z * GRID_BLOCK_X_COUNT) + x;
	}

	public Integer getX() {
		return x;
	}
			
	public Integer getZ() {
		return z;
	}

	public boolean equals(Object o) {
	    if(o == null)                return false;
	    if(!(o instanceof FlatCellCoordinate)) return false;

	    FlatCellCoordinate other = (FlatCellCoordinate) o;
	    return this.x == other.x && this.z == other.z;
	}
	
	public int hashCode(){
	    return getSortNumber();
	}
}
