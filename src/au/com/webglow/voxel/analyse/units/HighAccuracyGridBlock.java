package au.com.webglow.voxel.analyse.units;

import au.com.webglow.voxel.analyse.CalculationUtil;
import au.com.webglow.voxel.analyse.flash.GridBlockCoordinateStorage;

public class HighAccuracyGridBlock implements GridBlockPipeBuffer {
	private int blockZ; 
	private int blockX;
	private int blockY;
	private GridBlockPosition gridBlockPosition;
//	private HighAccuracyGridBlockNewlyOccupiedMonitor newlyOccupiedMonitor = new HighAccuracyGridBlockNewlyOccupiedMonitor();
	private boolean occupied = false;
	
	public static HighAccuracyGridBlock newInstance(int highAccuracyBlockZ, int highAccuracyBlockX, int highAccuracyBlockY) {
		GridBlockPosition gridBlockPosition = GridBlockCoordinateStorage.getInstance().getHighAccuracyGridBlockPosition(highAccuracyBlockX, highAccuracyBlockY, highAccuracyBlockZ);
		HighAccuracyGridBlock retVal = new HighAccuracyGridBlock();
		retVal.setGridBlockPosition(gridBlockPosition);
		retVal.setBlockZ(highAccuracyBlockZ);
		retVal.setBlockX(highAccuracyBlockX);
		retVal.setBlockY(highAccuracyBlockY);
		return retVal;
	}

	private HighAccuracyGridBlock() {}
	
	public void processOccupiedValue() {
//		newlyOccupiedMonitor.processOccupiedValue(occupied);
	}
	
//	public void prepareForNextFrame() {
//		occupied = false;
//		newlyOccupiedMonitor.prepareForNextFrame();
//	}
	
	public void reset() {
		occupied = false;
//		newlyOccupiedMonitor.initMonitoring();
	}

//	public HighAccuracyGridBlockNewlyOccupiedMonitor getNewlyOccupiedMonitor() {
//		return newlyOccupiedMonitor;
//	}
	
	public boolean isOccupied() {
		return occupied;
	}

	public void setOccupied(boolean occupied) {
		this.occupied = occupied;
	}

	public int getBlockZ() {
		return blockZ;
	}

	public void setBlockZ(int blockZ) {
		this.blockZ = blockZ;
	}

	public int getBlockX() {
		return blockX;
	}

	public void setBlockX(int blockX) {
		this.blockX = blockX;
	}

	public int getBlockY() {
		return blockY;
	}

	public void setBlockY(int blockY) {
		this.blockY = blockY;
	}

	public GridBlockPosition getGridBlockPosition() {
		return gridBlockPosition;
	}

	public void setGridBlockPosition(GridBlockPosition gridBlockPosition) {
		this.gridBlockPosition = gridBlockPosition;
	}

	@Override
	public byte[] getPipeBuffer() {
		return CalculationUtil.calcGridBlockPositionPipeBuffer(gridBlockPosition);
	}
}
