package au.com.webglow.voxel.analyse.units;

import java.util.ArrayList;
import java.util.List;

public class RegressionLineAccumulator {
	private int minX = -1;
	private int maxX = -1;
	private int minZ = -1;
	private int maxZ = -1;
	private int minY = -1;
	private int maxY = -1;
//	private float middleY = -1;
	private int sumX = 0;
	private int sumZ = 0;
	private float avgX = -1;
	private float avgZ = -1;
//	private float sumPointSlopeX = 0;
//	private float sumPointSlopeZ = 0;
//	private Slope avgSlope = null;
	private AveragedLine line = null;
	private List<Coordinate> points = new ArrayList<Coordinate>();
//	private List<Slope> pointSlopes = new ArrayList<Slope>();

	public void processPoint(Coordinate highAccuracyPoint) {
		if (minX == -1 || highAccuracyPoint.getX() < minX) {
			minX = highAccuracyPoint.getX();
		}
		if (maxX == -1 || highAccuracyPoint.getX() > maxX) {
			maxX = highAccuracyPoint.getX();
		}
		if (minZ == -1 || highAccuracyPoint.getZ() < minZ) {
			minZ = highAccuracyPoint.getZ();
		}
		if (maxZ == -1 || highAccuracyPoint.getZ() > maxZ) {
			maxZ = highAccuracyPoint.getZ();
		}
		if (minY == -1 || highAccuracyPoint.getY() < minY) {
			minY = highAccuracyPoint.getY();
		}
		if (maxY == -1 || highAccuracyPoint.getY() > maxY) {
			maxY = highAccuracyPoint.getY();
		}
		sumX += highAccuracyPoint.getX();
		sumZ += highAccuracyPoint.getZ();
		points.add(highAccuracyPoint);
	}
	
	public boolean isInitialized() {
		return points.size() >= 2;
	}
	
	public void calculateLine() {
//		middleY = ((float)(maxY + minY)) / ((float)2);
//		avgX = ((float)sumX) / ((float)points.size());
//		avgZ = ((float)sumZ) / ((float)points.size());
//		for (Coordinate point: points) {
//			float slopeX = (point.getX() - avgX) == 0 ? 0 : ((float)(point.getY() - middleY)) / ((float)(point.getX() - avgX));
//			float slopeZ = (point.getZ() - avgZ) == 0 ? 0 : ((float)(point.getY() - middleY)) / ((float)(point.getZ() - avgZ));
//			pointSlopes.add(Slope.newInstance(slopeX, slopeZ));
//		}
//		for (Slope slope: pointSlopes) {
//			sumPointSlopeX += slope.getRiseOverRunX();
//			sumPointSlopeZ += slope.getRiseOverRunZ();
//		}
//		avgSlope = Slope.newInstance(((float)sumPointSlopeX) / ((float)pointSlopes.size()), ((float)sumPointSlopeZ) / ((float)pointSlopes.size()));
//		float minRunX = avgSlope.getRiseOverRunX() == 0 ? 0 : ((float)(minY - middleY)) / avgSlope.getRiseOverRunX();
//		float maxRunX = avgSlope.getRiseOverRunX() == 0 ? 0 : ((float)(maxY - middleY)) / avgSlope.getRiseOverRunX();
//		float minRunZ = avgSlope.getRiseOverRunZ() == 0 ? 0 : ((float)(minY - middleY)) / avgSlope.getRiseOverRunZ();
//		float maxRunZ = avgSlope.getRiseOverRunZ() == 0 ? 0 : ((float)(maxY - middleY)) / avgSlope.getRiseOverRunZ();
//		line = AveragedLine.newInstance(
//				Coordinate.newInstance(((int)(avgX+minRunX)), minY, ((int)(avgZ+minRunZ))), 
//				Coordinate.newInstance(((int)(avgX+maxRunX)), maxY, ((int)(avgZ+maxRunZ))));
//		line = AveragedLine.newInstance(
//				Coordinate.newInstance((int)avgX, minY, (int)avgZ), 
//				Coordinate.newInstance((int)avgX, maxY, (int)avgZ),
//				Region.newInstance(minX, maxX, minZ, maxZ, minY, maxY));
	}
	
	public AveragedLine getLine() {
		return line;
	}

	public List<Coordinate> getPoints() {
		return points;
	}

	public void reset() {
		minX = -1;
		maxX = -1;
		minZ = -1;
		maxZ = -1;
		minY = -1;
		maxY = -1;
//		middleY = -1;
		sumX = 0;
		sumZ = 0;
		avgX = -1;
		avgZ = -1;
//		sumPointSlopeX = 0;
//		sumPointSlopeZ = 0;
//		avgSlope = null;
		line = null;
		points = new ArrayList<Coordinate>();
//		pointSlopes = new ArrayList<Slope>();
	}
}
