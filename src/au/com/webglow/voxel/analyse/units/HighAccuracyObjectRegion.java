package au.com.webglow.voxel.analyse.units;

public class HighAccuracyObjectRegion {
//implements RegionBoxPipeBuffer {
//	private GridBlockRange gridBlockRange;
//	private GridBlockRange highAccuracyGridBlockRange;
//	private GridBlockRange previousGridBlockRange;
//	private GridBlockRange previousHighAccuracyGridBlockRange;
//	private List<Coordinate> occupiedRegressionPoints = new ArrayList<Coordinate>();
//	private List<Coordinate> emptyRegressionPoints = new ArrayList<Coordinate>();
//
//	public static HighAccuracyObjectRegion newInstance(MinMaxValues gridBlockMinMaxX, MinMaxValues gridBlockMinMaxY, MinMaxValues gridBlockMinMaxZ) {
//		HighAccuracyObjectRegion retVal = new HighAccuracyObjectRegion();
//		retVal.setGridBlockRange(GridBlockRange.newInstance(
//				gridBlockMinMaxX.getMinValue(), 
//				gridBlockMinMaxY.getMinValue(), 
//				gridBlockMinMaxZ.getMinValue(), 
//				gridBlockMinMaxX.getMaxValue(), 
//				gridBlockMinMaxY.getMaxValue(), 
//				gridBlockMinMaxZ.getMaxValue()));
//		
//		int startX = gridBlockMinMaxX.getMinValue() * HIGH_ACCURACY_PER_GRID_BLOCK;
//		int endX = (gridBlockMinMaxX.getMaxValue() * HIGH_ACCURACY_PER_GRID_BLOCK) + HIGH_ACCURACY_PER_GRID_BLOCK-1;
//		int startY = gridBlockMinMaxY.getMinValue() * HIGH_ACCURACY_PER_GRID_BLOCK;
//		int endY = (gridBlockMinMaxY.getMaxValue() * HIGH_ACCURACY_PER_GRID_BLOCK) + HIGH_ACCURACY_PER_GRID_BLOCK-1;
//		int startZ = gridBlockMinMaxZ.getMinValue() * HIGH_ACCURACY_PER_GRID_BLOCK;
//		int endZ = (gridBlockMinMaxZ.getMaxValue() * HIGH_ACCURACY_PER_GRID_BLOCK) + HIGH_ACCURACY_PER_GRID_BLOCK-1;
//		retVal.setHighAccuracyGridBlockRange(GridBlockRange.newInstance(startX, startY, startZ, endX, endY, endZ));
//		
//		return retVal;
//	}
//	
//	public void updateBounds(int startX, int endX, int startZ, int endZ, int startY, int endY) {
//		setHighAccuracyGridBlockRange(GridBlockRange.newInstance(startX, startY, startZ, endX, endY, endZ));
//		setGridBlockRange(GridBlockRange.newInstance(
//				startX/HIGH_ACCURACY_PER_GRID_BLOCK, startY/HIGH_ACCURACY_PER_GRID_BLOCK, 
//				startZ/HIGH_ACCURACY_PER_GRID_BLOCK, endX/HIGH_ACCURACY_PER_GRID_BLOCK, 
//				endY/HIGH_ACCURACY_PER_GRID_BLOCK, endZ/HIGH_ACCURACY_PER_GRID_BLOCK));
//	}
//
//	@Override
//	public byte[] getPipeBuffer() {
//		short startZ = CalculationUtil.calcZCoordFromHighAccuracyBlockZ(getHighAccuracyGridBlockRange().getStartZ());
//		short endZ = CalculationUtil.calcZCoordFromHighAccuracyBlockZ(getHighAccuracyGridBlockRange().getEndZ());
//		short startX = CalculationUtil.calcXCoordFromHighAccuracyBlockX(getHighAccuracyGridBlockRange().getStartX());
//		short endX = CalculationUtil.calcXCoordFromHighAccuracyBlockX(getHighAccuracyGridBlockRange().getEndX());
//		short startY = CalculationUtil.calcYCoordFromHighAccuracyBlockY(getHighAccuracyGridBlockRange().getStartY());
//		short endY = CalculationUtil.calcYCoordFromHighAccuracyBlockY(getHighAccuracyGridBlockRange().getEndY());
//		return CalculationUtil.calcRegionBoxPipeBuffer(startX, endX, startY, endY, startZ, endZ);
//	}
//	
//	public void addOccupiedRegressionPoint(Coordinate point) {
//		occupiedRegressionPoints.add(point);
//	}
//	
//	public void addEmptyRegressionPoint(Coordinate point) {
//		emptyRegressionPoints.add(point);
//	}
//	
//	public GridBlockRange getGridBlockRange() {
//		return gridBlockRange;
//	}
//
//	public void setGridBlockRange(GridBlockRange gridBlockRange) {
//		this.gridBlockRange = gridBlockRange;
//	}
//
//	public GridBlockRange getHighAccuracyGridBlockRange() {
//		return highAccuracyGridBlockRange;
//	}
//
//	public void setHighAccuracyGridBlockRange(GridBlockRange highAccuracyGridBlockRange) {
//		this.highAccuracyGridBlockRange = highAccuracyGridBlockRange;
//	}
//	
//	public GridBlockRange getPreviousGridBlockRange() {
//		return previousGridBlockRange;
//	}
//
//	public void setPreviousGridBlockRange(GridBlockRange previousGridBlockRange) {
//		this.previousGridBlockRange = previousGridBlockRange;
//	}
//
//	public GridBlockRange getPreviousHighAccuracyGridBlockRange() {
//		return previousHighAccuracyGridBlockRange;
//	}
//
//	public void setPreviousHighAccuracyGridBlockRange(GridBlockRange previousHighAccuracyGridBlockRange) {
//		this.previousHighAccuracyGridBlockRange = previousHighAccuracyGridBlockRange;
//	}
//
//	public List<Coordinate> getOccupiedRegressionPoints() {
//		return occupiedRegressionPoints;
//	}
//
//	public void setOccupiedRegressionPoints(List<Coordinate> occupiedRegressionPoints) {
//		this.occupiedRegressionPoints = occupiedRegressionPoints;
//	}
//
//	public List<Coordinate> getEmptyRegressionPoints() {
//		return emptyRegressionPoints;
//	}
//
//	public void setEmptyRegressionPoints(List<Coordinate> emptyRegressionPoints) {
//		this.emptyRegressionPoints = emptyRegressionPoints;
//	}
//	
//	@Override
//	public String toString() {
//	return String.format("minX=%d, maxX=%d, minZ=%d, maxZ=%d, minY=%d, maxY=%d",
//			highAccuracyGridBlockRange.getStartX(), 
//			highAccuracyGridBlockRange.getEndX(), 
//			highAccuracyGridBlockRange.getStartZ(), 
//			highAccuracyGridBlockRange.getEndZ(), 
//			highAccuracyGridBlockRange.getStartY(), 
//			highAccuracyGridBlockRange.getEndY());
//	}
}