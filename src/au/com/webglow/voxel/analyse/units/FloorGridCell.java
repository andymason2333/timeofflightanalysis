package au.com.webglow.voxel.analyse.units;

import au.com.webglow.voxel.analyse.helper.FloorGridCellObjectMonitor;

public class FloorGridCell {
	private FlatCellCoordinate coordinate;
	private FloorGridCellFieldOfViewRange fieldOfViewRange;
	private boolean highlightObjectDetected = false;
	private boolean highlightHighAccuracy = false;
	private int highAccuracyOccupiedPointCount = 0;
	private int highAccuracyEmptyPointCount = 0;
	private boolean confirmedObjectLocation = false; 
	private FloorGridCellObjectMonitor floorGridCellObjectMonitor = new FloorGridCellObjectMonitor();
	
	public static FloorGridCell newInstance(int x, int z) {
		return new FloorGridCell(x, z);
	}
	
	public static FloorGridCell newCloneInstance(FloorGridCell original) {
		FloorGridCell retVal = new FloorGridCell();
		retVal.setCoordinate(original.getCoordinate());
		retVal.setFieldOfViewRange(original.getFieldOfViewRange());
		return retVal;
	}
	
	private FloorGridCell() {}
	
	private FloorGridCell(int x, int z) {
		coordinate = FlatCellCoordinate.newInstance(x, z);
		fieldOfViewRange = FloorGridCellFieldOfViewRange.newInstance(x, z);
	}
	
	public FloorGridCell clone() {
		FloorGridCell retVal = newCloneInstance(this);
		retVal.highlightObjectDetected = floorGridCellObjectMonitor.isObjectDetectedHighlighted();
		retVal.highlightHighAccuracy = floorGridCellObjectMonitor.isHighAccuracyHighlighted();
		retVal.highAccuracyOccupiedPointCount = floorGridCellObjectMonitor.getHighAccuracyOccupiedPointCount();
		retVal.highAccuracyEmptyPointCount = floorGridCellObjectMonitor.getHighAccuracyEmptyPointCount();
		return retVal;
	}
	
	public void reset() {
		highlightObjectDetected = false;
		highlightHighAccuracy = false;
		highAccuracyOccupiedPointCount = 0;
		highAccuracyEmptyPointCount = 0;
		floorGridCellObjectMonitor.reset();
	}

	public FlatCellCoordinate getCoordinate() {
		return coordinate;
	}

	public void setCoordinate(FlatCellCoordinate coordinate) {
		this.coordinate = coordinate;
	}

	public FloorGridCellFieldOfViewRange getFieldOfViewRange() {
		return fieldOfViewRange;
	}

	public void setFieldOfViewRange(FloorGridCellFieldOfViewRange fieldOfViewRange) {
		this.fieldOfViewRange = fieldOfViewRange;
	}

	public boolean isHighlightObjectDetected() {
		return highlightObjectDetected;
	}

	public boolean isHighlightHighAccuracy() {
		return highlightHighAccuracy;
	}

	public FloorGridCellObjectMonitor getFloorGridCellObjectMonitor() {
		return floorGridCellObjectMonitor;
	}

	public int getHighAccuracyOccupiedPointCount() {
		return highAccuracyOccupiedPointCount;
	}

	public int getHighAccuracyEmptyPointCount() {
		return highAccuracyEmptyPointCount;
	}

	public void setConfirmedObjectLocation(boolean confirmedObjectLocation) {
		this.confirmedObjectLocation = confirmedObjectLocation;
	}

	public boolean isConfirmedObjectLocation() {
		return confirmedObjectLocation;
	}
}
