package au.com.webglow.voxel.analyse.units;

public interface GridBlockPipeBuffer {
	byte[] getPipeBuffer();
}
