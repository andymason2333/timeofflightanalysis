package au.com.webglow.voxel.analyse.units;

public class FloorGridRegionCellHeights {
	private int minHeight;
	private int maxHeight;
	
	public static FloorGridRegionCellHeights newInstance(int minHeight, int maxHeight) {
		return new FloorGridRegionCellHeights(minHeight, maxHeight);
	}
	
	private FloorGridRegionCellHeights(int minHeight, int maxHeight) {
		this.minHeight = minHeight;
		this.maxHeight = maxHeight;
	}

	public int getMinHeight() {
		return minHeight;
	}

	public void setMinHeight(int minHeight) {
		this.minHeight = minHeight;
	}

	public int getMaxHeight() {
		return maxHeight;
	}

	public void setMaxHeight(int maxHeight) {
		this.maxHeight = maxHeight;
	}
}
