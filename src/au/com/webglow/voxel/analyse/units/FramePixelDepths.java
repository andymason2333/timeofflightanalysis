package au.com.webglow.voxel.analyse.units;

public class FramePixelDepths {
	private int[][] frameDepths;
	private int[][] highAccuracyFrameDepths;
	private FrameDepthChange[][] frameDepthChanges;
	
	public int[][] getFrameDepths() {
		return frameDepths;
	}
	
	public void setFrameDepths(int[][] frameDepths) {
		this.frameDepths = frameDepths;
	}

	public int[][] getHighAccuracyFrameDepths() {
		return highAccuracyFrameDepths;
	}
	
	public void setHighAccuracyFrameDepths(int[][] highAccuracyFrameDepths) {
		this.highAccuracyFrameDepths = highAccuracyFrameDepths;
	}

	public FrameDepthChange[][] getFrameDepthChanges() {
		return frameDepthChanges;
	}

	public void setFrameDepthChanges(FrameDepthChange[][] frameDepthChanges) {
		this.frameDepthChanges = frameDepthChanges;
	}
}
