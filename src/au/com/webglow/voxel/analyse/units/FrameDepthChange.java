package au.com.webglow.voxel.analyse.units;

public enum FrameDepthChange {
	SAME, NEARER, FURTHER;
}
