package au.com.webglow.voxel.analyse.units;

import java.util.List;

public class FlatRegion {
	private int minX;
	private int maxX;
	private int minZ;
	private int maxZ;
	private List<FlatCellCoordinate> regionCoordinates;
	
	public static FlatRegion newInstance(int minX, int maxX, int minZ, int maxZ, List<FlatCellCoordinate> regionCoordinates) {
		return new FlatRegion(minX, maxX, minZ, maxZ, regionCoordinates);
	}
	
	public static FlatRegion newInstance(int minX, int maxX, int minZ, int maxZ) {
		return new FlatRegion(minX, maxX, minZ, maxZ);
	}
	
	protected FlatRegion(int minX, int maxX, int minZ, int maxZ, List<FlatCellCoordinate> regionCoordinates) {
		this.minX = minX;
		this.maxX = maxX;
		this.minZ = minZ;
		this.maxZ = maxZ;
		this.regionCoordinates = regionCoordinates;
	}
	
	protected FlatRegion(int minX, int maxX, int minZ, int maxZ) {
		this.minX = minX;
		this.maxX = maxX;
		this.minZ = minZ;
		this.maxZ = maxZ;
	}
	
	protected FlatRegion(FlatRegion flatRegion) {
		this.minX = flatRegion.getMinX();
		this.maxX = flatRegion.getMaxX();
		this.minZ = flatRegion.getMinZ();
		this.maxZ = flatRegion.getMaxZ();
		this.regionCoordinates = flatRegion.getRegionCoordinates();
	}
	
	public void addRegionCoordinate(FlatCellCoordinate coordinate) {
		regionCoordinates.add(coordinate);
	}
	
	public int getMinX() {
		return minX;
	}

	public void setMinX(int minX) {
		this.minX = minX;
	}

	public int getMaxX() {
		return maxX;
	}

	public void setMaxX(int maxX) {
		this.maxX = maxX;
	}

	public int getMinZ() {
		return minZ;
	}

	public void setMinZ(int minZ) {
		this.minZ = minZ;
	}

	public int getMaxZ() {
		return maxZ;
	}

	public void setMaxZ(int maxZ) {
		this.maxZ = maxZ;
	}

	public List<FlatCellCoordinate> getRegionCoordinates() {
		return regionCoordinates;
	}
}
