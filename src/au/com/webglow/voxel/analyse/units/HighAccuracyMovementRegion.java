package au.com.webglow.voxel.analyse.units;

public class HighAccuracyMovementRegion {
//	implements RegionBoxPipeBuffer {
//
//	private GridBlockRange gridBlockRange;
//	private GridBlockRange highAccuracyGridBlockRange;
//
//	public static HighAccuracyMovementRegion newInstance(MinMaxValues gridBlockMinMaxX, MinMaxValues gridBlockMinMaxY, MinMaxValues gridBlockMinMaxZ) {
//		HighAccuracyMovementRegion retVal = new HighAccuracyMovementRegion();
//		retVal.setGridBlockRange(GridBlockRange.newInstance(
//				gridBlockMinMaxX.getMinValue(), 
//				gridBlockMinMaxY.getMinValue(), 
//				gridBlockMinMaxZ.getMinValue(), 
//				gridBlockMinMaxX.getMaxValue(), 
//				gridBlockMinMaxY.getMaxValue(), 
//				gridBlockMinMaxZ.getMaxValue()));
//		int startX = gridBlockMinMaxX.getMinValue() * HIGH_ACCURACY_PER_GRID_BLOCK;
//		int endX = (gridBlockMinMaxX.getMaxValue() * HIGH_ACCURACY_PER_GRID_BLOCK) + HIGH_ACCURACY_PER_GRID_BLOCK-1;
//		int startY = gridBlockMinMaxY.getMinValue() * HIGH_ACCURACY_PER_GRID_BLOCK;
//		int endY = (gridBlockMinMaxY.getMaxValue() * HIGH_ACCURACY_PER_GRID_BLOCK) + HIGH_ACCURACY_PER_GRID_BLOCK-1;
//		int startZ = gridBlockMinMaxZ.getMinValue() * HIGH_ACCURACY_PER_GRID_BLOCK;
//		int endZ = (gridBlockMinMaxZ.getMaxValue() * HIGH_ACCURACY_PER_GRID_BLOCK) + HIGH_ACCURACY_PER_GRID_BLOCK-1;
//		retVal.setHighAccuracyGridBlockRange(GridBlockRange.newInstance(startX, startY, startZ, endX, endY, endZ));
//		return retVal;
//	}
//
//	public GridBlockRange getGridBlockRange() {
//		return gridBlockRange;
//	}
//
//	public void setGridBlockRange(GridBlockRange gridBlockRange) {
//		this.gridBlockRange = gridBlockRange;
//	}
//
//	public GridBlockRange getHighAccuracyGridBlockRange() {
//		return highAccuracyGridBlockRange;
//	}
//
//	public void setHighAccuracyGridBlockRange(GridBlockRange highAccuracyGridBlockRange) {
//		this.highAccuracyGridBlockRange = highAccuracyGridBlockRange;
//	}
//
//	@Override
//	public byte[] getPipeBuffer() {
//		short startZ = CalculationUtil.calcZCoordFromHighAccuracyBlockZ(getHighAccuracyGridBlockRange().getStartZ());
//		short endZ = CalculationUtil.calcZCoordFromHighAccuracyBlockZ(getHighAccuracyGridBlockRange().getEndZ());
//		short startX = CalculationUtil.calcXCoordFromHighAccuracyBlockX(getHighAccuracyGridBlockRange().getStartX());
//		short endX = CalculationUtil.calcXCoordFromHighAccuracyBlockX(getHighAccuracyGridBlockRange().getEndX());
//		short startY = CalculationUtil.calcYCoordFromHighAccuracyBlockY(getHighAccuracyGridBlockRange().getStartY());
//		short endY = CalculationUtil.calcYCoordFromHighAccuracyBlockY(getHighAccuracyGridBlockRange().getEndY());
//		return CalculationUtil.calcRegionBoxPipeBuffer(startX, endX, startY, endY, startZ, endZ);
//	}
}
