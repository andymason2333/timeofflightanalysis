package au.com.webglow.voxel.analyse.units;

public class Slope {
	private float riseOverRunX;
	private float riseOverRunZ;
	
	public static Slope newInstance(float riseOverRunX, float riseOverRunZ) {
		return new Slope(riseOverRunX, riseOverRunZ);
	}
	
	private Slope(float riseOverRunX, float riseOverRunZ) {
		this.riseOverRunX = riseOverRunX;
		this.riseOverRunZ = riseOverRunZ;
	}

	public float getRiseOverRunX() {
		return riseOverRunX;
	}

	public float getRiseOverRunZ() {
		return riseOverRunZ;
	}
}
