package au.com.webglow.voxel.analyse.units;

import au.com.webglow.voxel.analyse.helper.AbleToFormNewlyOccupiedStatus;
import au.com.webglow.voxel.analyse.helper.FrameDepthChangeMonitorManager;

public class FramePixelHistory {
	private int previousFrameDepth = -1;
	private int frameDepth = -1;
	private FrameDepthChangeMonitorManager frameDepthChangeMonitorManager = FrameDepthChangeMonitorManager.newInstance();
	
	public void processFrameDepth(int frameDepth, FrameDepthChange frameDepthChange) {
		this.frameDepth = frameDepth;
		if (previousFrameDepth != -1) {
			frameDepthChangeMonitorManager.processFrameDepthChange(frameDepthChange, frameDepth-previousFrameDepth);	
		}
	}

	public void prepareForNextFrame() {
		previousFrameDepth = frameDepth;
	}

	public int getFrameDepth() {
		return frameDepth;
	}

	public int getPreviousFrameDepth() {
		return previousFrameDepth;
	}

	public AbleToFormNewlyOccupiedStatus getAbleToFormNewlyOccupiedStatus() {
		return frameDepthChangeMonitorManager.getAbleToFormNewlyOccupiedStatus();
	}
}
