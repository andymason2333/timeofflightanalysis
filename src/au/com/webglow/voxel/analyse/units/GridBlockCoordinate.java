package au.com.webglow.voxel.analyse.units;

import static au.com.webglow.voxel.Constants.GRID_BLOCK_Y_COUNT;
import static au.com.webglow.voxel.Constants.GRID_BLOCK_Z_COUNT;
public class GridBlockCoordinate {
	private int z; 
	private int x; 
	private int y;

	public static GridBlockCoordinate newInstance(int z, int x, int y) {
		return new GridBlockCoordinate(z, x, y);
	}

	private GridBlockCoordinate(int z, int x, int y) {
		this.z = z;
		this.x = x;
		this.y = y;
	}

	public int getZ() {
		return z;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public boolean equals(Object o) {
	    if(o == null)                return false;
	    if(!(o instanceof GridBlockCoordinate)) return false;

	    GridBlockCoordinate other = (GridBlockCoordinate) o;
	    return this.z == other.z && this.x == other.x && this.y == other.y;
	}
	
	public int hashCode(){
	    return z + (y * GRID_BLOCK_Z_COUNT) + (x * GRID_BLOCK_Z_COUNT * GRID_BLOCK_Y_COUNT);
	  }
}
