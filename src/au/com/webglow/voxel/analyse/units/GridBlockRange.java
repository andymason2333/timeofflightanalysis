package au.com.webglow.voxel.analyse.units;

public class GridBlockRange {
	private int startX;
	private int startY;
	private int startZ;
	private int endX;
	private int endY;
	private int endZ;
	
	public static GridBlockRange newInstance(int startX, int startY, int startZ, int endX, int endY, int endZ) {
		GridBlockRange retVal = new GridBlockRange();
		retVal.setStartX(startX);
		retVal.setStartY(startY);
		retVal.setStartZ(startZ);
		retVal.setEndX(endX);
		retVal.setEndY(endY);
		retVal.setEndZ(endZ);
		return retVal;
	}
	
	public int getStartX() {
		return startX;
	}
	
	public void setStartX(int startX) {
		this.startX = startX;
	}
	
	public int getStartY() {
		return startY;
	}
	
	public void setStartY(int startY) {
		this.startY = startY;
	}
	
	public int getStartZ() {
		return startZ;
	}
	public void setStartZ(int startZ) {
		this.startZ = startZ;
	}
	public int getEndX() {
		return endX;
	}
	
	public void setEndX(int endX) {
		this.endX = endX;
	}
	
	public int getEndY() {
		return endY;
	}
	
	public void setEndY(int endY) {
		this.endY = endY;
	}
	
	public int getEndZ() {
		return endZ;
	}
	
	public void setEndZ(int endZ) {
		this.endZ = endZ;
	}
	
	@Override
	public String toString() {
		return String.format("minX=%d, maxX=%d, minY=%d, maxY=%d, minZ=%d, maxZ=%d", 
				getStartX(), getEndX(), getStartY(), getEndY(), getStartZ(), getEndZ());
	}
}
