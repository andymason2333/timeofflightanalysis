package au.com.webglow.voxel;

public class FrameCounter {
	private static int framesSincePipePositions = 0;
	private static int framesSinceDetermineFloorPositions = 0;

	public static void incrementFrameCount() {
		framesSincePipePositions++;
		framesSinceDetermineFloorPositions++;
	}

	public static int getFramesSincePipePositions() {
		return framesSincePipePositions;
	}

	public static int getFramesSinceDetermineFloorPositions() {
		return framesSinceDetermineFloorPositions;
	}

	public static void resetFramesSincePipePositions() {
		framesSincePipePositions = 0;
	}

	public static void resetFramesSinceDetermineFloorPositions() {
		framesSinceDetermineFloorPositions = 0;
	}
}
