package au.com.webglow.voxel.gui;

import static au.com.webglow.voxel.Constants.SENSOR_COL_SIZE;
import static au.com.webglow.voxel.Constants.SENSOR_ROW_SIZE;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JPanel;

import au.com.webglow.voxel.Constants;
import au.com.webglow.voxel.analyse.units.FramePixelHistory;
import au.com.webglow.voxel.analyse.units.GridBlock;

public class TwoDFrameDisplay extends JPanel {
	private static final long serialVersionUID = 1L;
	private Color[][] frameDepthColors = new Color[Constants.SENSOR_ROW_SIZE][Constants.SENSOR_COL_SIZE];
	
	public TwoDFrameDisplay() {
		Dimension size = new Dimension(800, 800);
		setMinimumSize(size);
		setPreferredSize(size);
		setSize(size);
	}

	public void paint(Graphics g) {
		for (int row = 0; row < SENSOR_ROW_SIZE; row++) {
			for (int col = 0; col < SENSOR_COL_SIZE; col++) {
				int rowStart = (row * 2);
				int colStart = (col * 2);
				g.setColor(frameDepthColors[row][col]);
				g.fillRect(colStart, rowStart, 2, 2);
			}
		}
	}

	public void buildForegroundObjectsCells(FramePixelHistory[][] framePixelHistory, GridBlock[][][] gridBlocks) {
		for (int row = 0; row < SENSOR_ROW_SIZE; row++) {
			for (int col = 0; col < SENSOR_COL_SIZE; col++) {
				switch (framePixelHistory[row][col].getAbleToFormNewlyOccupiedStatus()) {
					case ABLE_TO_FORM_NEWLY_OCCUPIED:
						frameDepthColors[row][col] = Color.GREEN;
						break;
					case NOT_ABLE_TO_FORM_NEWLY_OCCUPIED:
						frameDepthColors[row][col] = Color.WHITE;
						break;
				}
			}
		}
	}
}
