package au.com.webglow.voxel.gui;

import static au.com.webglow.voxel.Constants.FLOOR_GRID_CELL_COL_COUNT;
import static au.com.webglow.voxel.Constants.FLOOR_GRID_CELL_DEPTH_COUNT;
import static au.com.webglow.voxel.Constants.GRID_BLOCK_X_COUNT;
import static au.com.webglow.voxel.Constants.GRID_BLOCK_Y_COUNT;
import static au.com.webglow.voxel.Constants.GRID_BLOCK_Z_COUNT;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.io.File;

import javax.swing.JPanel;

import au.com.webglow.voxel.analyse.units.GridBlock;

public class FloorGridDisplay extends JPanel {
	private static final long serialVersionUID = 1L;
//	private boolean[][] foregroundObjectsCells = new boolean[FLOOR_GRID_CELL_COL_COUNT][FLOOR_GRID_CELL_DEPTH_COUNT];
	private Color[][] cellColours = new Color[FLOOR_GRID_CELL_COL_COUNT][FLOOR_GRID_CELL_DEPTH_COUNT];
//	private FloorGridDisplayHighAccuracyCell[][] highAccuracyObjectsCells = new FloorGridDisplayHighAccuracyCell[FLOOR_GRID_CELL_COL_COUNT][FLOOR_GRID_CELL_DEPTH_COUNT];
	private boolean needToSaveImage = false;
	
	public FloorGridDisplay() {
		Dimension size = new Dimension(800, 800);
		setMinimumSize(size);
		setPreferredSize(size);
		setSize(size);
		
//		for (int x = 0; x < FLOOR_GRID_CELL_COL_COUNT; x++) {
//			for (int z = 0; z < FLOOR_GRID_CELL_DEPTH_COUNT; z++) {
//				foregroundObjectsCells[x][z] = false;
//			}
//		}
//		for (int x = 0; x < FLOOR_GRID_CELL_COL_COUNT; x++) {
//			for (int z = 0; z < FLOOR_GRID_CELL_DEPTH_COUNT; z++) {
//				highAccuracyObjectsCells[x][z] = new FloorGridDisplayHighAccuracyCell();
//			}
//		}
		
//		deleteFrameImages();
	}
	
	public void saveFrameImage() {
		if (needToSaveImage) {
//			BufferedImage image = new BufferedImage(getWidth(),getHeight(), BufferedImage.TYPE_INT_RGB);
//			Graphics2D g2 = image.createGraphics();
//			paint(g2);
//			try{
//				ImageIO.write(image, "png", new File("C:\\Users\\andym\\Documents\\webglow\\floor_grid_images\\"+System.currentTimeMillis()+".png"));
//			} catch (Exception e) {
//				e.printStackTrace();
//			}	
		}
	}
	
	public void deleteFrameImages() {
		File folder = new File("C:\\Users\\andym\\Documents\\webglow\\floor_grid_images");
	    File[] files = folder.listFiles();
	    if (files != null) {
	        for(File f: files) {
	            if (!f.isDirectory()) {
	                f.delete();
	            }
	        }
	    }
	}

	public void paint(Graphics g) {
		for (int x = 0; x < FLOOR_GRID_CELL_COL_COUNT; x++) {
			for (int z = FLOOR_GRID_CELL_DEPTH_COUNT-1; z >= 0 ; z--) {
				int xStart = (x * 20);
				int zStart = (z * 20);
//				if (highAccuracyObjectsCells[x][z].isHighlight()) {
//					g.setColor(Color.BLUE);
//					g.fillRect(xStart+1, zStart+1, 18, 18);
//					g.setFont(new Font("Arial", Font.PLAIN, 9));
//					if (highAccuracyObjectsCells[x][z].isHighlight()) {
//						g.setColor(Color.GREEN);
//						g.drawString(Integer.toString(highAccuracyObjectsCells[x][z].getOccupiedPointCount()), xStart+7, zStart+9);
//						g.setColor(Color.RED);
//						g.drawString(Integer.toString(highAccuracyObjectsCells[x][z].getEmptyPointCount()), xStart+7, zStart+17);
//					}
//				   
//				} else if (foregroundObjectsCells[x][z]) {
//					g.setColor(Color.GREEN);
//					g.fillRect(xStart, zStart, 20, 20);
//				} else {
//					g.setColor(Color.WHITE);
//					g.fillRect(xStart, zStart, 20, 20);
//				}
				
				g.setColor(cellColours[x][z]);
				g.fillRect(xStart, zStart, 20, 20);
			}
		}
	}
	
	public void buildGridBlockCells(GridBlock[][][] gridBlocks) {
		for (int x = 0; x < GRID_BLOCK_X_COUNT; x++) {
			for (int z = 0; z < GRID_BLOCK_Z_COUNT; z++) {
				int newlyOccupiedCount = 0;
				for (int y = 0; y < GRID_BLOCK_Y_COUNT; y++) {
					if (gridBlocks[x][y][z].getOccupiedMonitor().isNewlyOccupied()) {
						newlyOccupiedCount++;
					}
				}
				cellColours[x][z] = convertNewlyOccupiedCountToColor(newlyOccupiedCount);
			}
		}
	}

	private Color convertNewlyOccupiedCountToColor(int newlyOccupiedCount) {
		return new Color(Math.min(255, (newlyOccupiedCount*30)), 0, 0);
	}

//	public void buildForegroundObjectsCells(List<FloorGridRegion> regions) {
//		for (int x = 0; x < FLOOR_GRID_CELL_COL_COUNT; x++) {
//			for (int z = 0; z < FLOOR_GRID_CELL_DEPTH_COUNT; z++) {
//				foregroundObjectsCells[x][z] = false;
//			}
//		}
//		for (FloorGridRegion region: regions) {
//			for (FlatCellCoordinate flatCellCoordinate: region.getRegionCoordinates()) {
//				foregroundObjectsCells[flatCellCoordinate.getX()][flatCellCoordinate.getZ()] = true;
//			}
//		}
//	}

//	public void buildHighAccuracyObjectsCells(FloorGridCell[][] floorGridCells) {
//		needToSaveImage = false;
//		
//		for (int x = 0; x < FLOOR_GRID_CELL_COL_COUNT; x++) {
//			for (int z = 0; z < FLOOR_GRID_CELL_DEPTH_COUNT; z++) {
//				if (floorGridCells[x][z].isHighlightHighAccuracy()) {
//					needToSaveImage = true;
//					highAccuracyObjectsCells[x][z].setHighlight(true);
//					highAccuracyObjectsCells[x][z].setOccupiedPointCount(floorGridCells[x][z].getHighAccuracyOccupiedPointCount());
//					highAccuracyObjectsCells[x][z].setEmptyPointCount(floorGridCells[x][z].getHighAccuracyEmptyPointCount());
//				} else {
//					highAccuracyObjectsCells[x][z].setHighlight(false);
//				}
//			}
//		}
//	}
}
