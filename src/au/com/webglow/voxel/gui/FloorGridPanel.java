package au.com.webglow.voxel.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import au.com.webglow.voxel.analyse.units.GridBlock;

public class FloorGridPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private FloorGridDisplay floorGridDisplay = new FloorGridDisplay();
	public static JCheckBox showForegroundObjects = new JCheckBox();
	public static JCheckBox showHighAccuracyObjects = new JCheckBox();
	
	public FloorGridPanel() {
		this.add(buildFilteringPanel(), BorderLayout.NORTH);
		this.add(floorGridDisplay, BorderLayout.CENTER);
	}
	
	private JPanel buildFilteringPanel() {
		JPanel retVal = new JPanel(new GridLayout(2, 1));
		JPanel row1 = new JPanel(new FlowLayout());
		
		showForegroundObjects.setSelected(false);
		row1.add(new JLabel("Foreground Objects:"));
		row1.add(showForegroundObjects);
		
		showHighAccuracyObjects.setSelected(false);
		row1.add(new JLabel("High Accuracy Objects:"));
		row1.add(showHighAccuracyObjects);
		
		retVal.add(row1);
		return retVal;
	}

//	public void processFloorGrid(List<FloorGridRegion> foregroundObjects, FloorGridCell[][] floorGridCells) {
//		if (showForegroundObjects.isSelected()) {
//			floorGridDisplay.buildForegroundObjectsCells(foregroundObjects);	//xxx; // show these in more detail
//		}
//		if (showHighAccuracyObjects.isSelected()) {
//			floorGridDisplay.buildHighAccuracyObjectsCells(floorGridCells);	
//		}
//		if (showForegroundObjects.isSelected() || showHighAccuracyObjects.isSelected()) {
//			floorGridDisplay.repaint();
//		}
//		if (showHighAccuracyObjects.isSelected()) {
//			floorGridDisplay.saveFrameImage();
//		}
//	}

	public void processGridBlocks(GridBlock[][][] gridBlocks) {
		floorGridDisplay.buildGridBlockCells(gridBlocks);
		floorGridDisplay.repaint();
	}
}
