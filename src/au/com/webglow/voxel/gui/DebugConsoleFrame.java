package au.com.webglow.voxel.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;

import au.com.webglow.voxel.analyse.units.FramePixelHistory;
import au.com.webglow.voxel.analyse.units.GridBlock;

public class DebugConsoleFrame extends JFrame {	
	private static final long serialVersionUID = 1L;
	private static FrameDisplayPanel frameDisplayPanel = new FrameDisplayPanel();
	private static FloorGridPanel floorGridPanel = new FloorGridPanel();
	private static LoggingPanel loggingPanel = new LoggingPanel();
	private static File logFile;
	private static BufferedWriter writer;	
	
	public static void showFrame() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				DebugConsoleFrame retVal = new DebugConsoleFrame();
				retVal.setVisible(true);
			}
		});
	}
	
	private DebugConsoleFrame() {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLayout(new BorderLayout());
		this.setPreferredSize(new Dimension(940, 840));
		
		try {
			logFile = new File("C:\\Users\\andym\\Documents\\webglow\\log.txt");
			writer = new BufferedWriter(new FileWriter(logFile));
		} catch (IOException ioe) {
			appendLogTextLn("Error creating log file");
		}
		
		JTabbedPane tabbedPane = new JTabbedPane();
		tabbedPane.addTab("Frame", frameDisplayPanel);
		tabbedPane.addTab("FloorView", floorGridPanel);
		tabbedPane.addTab("Logging", loggingPanel);
		this.getContentPane().add(tabbedPane, BorderLayout.CENTER);	
		this.pack();
	}
	
	public static void update2dFrameDisplay(FramePixelHistory[][] framePixelHistory, GridBlock[][][] gridBlocks) {
		frameDisplayPanel.processFrameDepths(framePixelHistory, gridBlocks);
	}
	
	public static void updateGridBlockDistributionDisplay(GridBlock[][][] gridBlocks) {
		floorGridPanel.processGridBlocks(gridBlocks);
	}

	public static void appendLogText(String log) {		
		try {
			loggingPanel.getLogsArea().append(log);	
		} catch (Exception e) {
			loggingPanel.getLogsArea().append("BAD LOG");
		}
	}
	
	public static void appendLogTextLn(String log) {
		try {
			loggingPanel.getLogsArea().append(log+"\n");	
		} catch (Exception e) {
			loggingPanel.getLogsArea().append("BAD LOG");
		}		
	}
	
	public static void appendLogTextLn(Exception e) {
		try {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);	
			appendLogTextLn(sw.toString());	
		} catch (Exception ex) {
			loggingPanel.getLogsArea().append("BAD LOG");
		}		
	}
	
	public static void appendFileTextLn(String log) {
		try {
			writer.append(log + "\n");
		} catch (IOException ioe) {
			loggingPanel.getLogsArea().append("BAD LOG FILE");
		}
	}
}
