package au.com.webglow.voxel.gui;

import java.awt.BorderLayout;

import javax.swing.JPanel;

import au.com.webglow.voxel.analyse.units.FramePixelHistory;
import au.com.webglow.voxel.analyse.units.GridBlock;

public class FrameDisplayPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private TwoDFrameDisplay twoDFrameDisplay = new TwoDFrameDisplay();
	
	public FrameDisplayPanel() {
		this.add(twoDFrameDisplay, BorderLayout.CENTER);
	}
	
	public void processFrameDepths(FramePixelHistory[][] framePixelHistory, GridBlock[][][] gridBlocks) {
		twoDFrameDisplay.buildForegroundObjectsCells(framePixelHistory, gridBlocks);
		twoDFrameDisplay.repaint();
	}
}
