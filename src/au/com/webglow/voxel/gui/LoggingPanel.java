package au.com.webglow.voxel.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class LoggingPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	public static JCheckBox showOccupiedBlocks = new JCheckBox();
	public static JCheckBox showNewlyOccupiedBlocks = new JCheckBox();
	public static JCheckBox showObjectDetectedBlocks = new JCheckBox();
	public static JCheckBox showHighAccuracyOccupiedBlocks = new JCheckBox();
	public static JCheckBox showHighAccuracyExitedBlocks = new JCheckBox();
	public static JCheckBox showHighAccuracyNewlyOccupiedBlocks = new JCheckBox();
	public static JCheckBox showHighAccuracyNewlyEmptyBlocks = new JCheckBox();
	public static JCheckBox showOccupiedAveragedPoint = new JCheckBox();
	public static JCheckBox showEmptyAveragedPoint = new JCheckBox();
	private JTextArea logsArea = new JTextArea();

	public LoggingPanel() {
		JScrollPane scroll = new JScrollPane(logsArea, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);		
		this.setLayout(new BorderLayout());
		this.add(buildBlockDisplayFilteringPanel(), BorderLayout.NORTH);
		this.add(scroll, BorderLayout.CENTER);	
	}
	
	private JPanel buildBlockDisplayFilteringPanel() {
		JPanel retVal = new JPanel(new GridLayout(2, 1));
		JPanel row1 = new JPanel(new FlowLayout());
		JPanel row2 = new JPanel(new FlowLayout());
		
		showOccupiedBlocks.setSelected(false);
		row1.add(new JLabel("Occupied:"));
		row1.add(showOccupiedBlocks);
		
		showNewlyOccupiedBlocks.setSelected(false);
		row1.add(new JLabel("New-Occupied:"));
		row1.add(showNewlyOccupiedBlocks);
		
		showObjectDetectedBlocks.setSelected(false);
		row1.add(new JLabel("Object-detected:"));
		row1.add(showObjectDetectedBlocks);
		
		showHighAccuracyNewlyOccupiedBlocks.setSelected(false);
		row1.add(new JLabel("High-Accuracy New-Occupied:"));
		row1.add(showHighAccuracyNewlyOccupiedBlocks);
		
		showHighAccuracyNewlyEmptyBlocks.setSelected(false);
		row1.add(new JLabel("High-Accuracy New-Empty:"));
		row1.add(showHighAccuracyNewlyEmptyBlocks);
		
		
		showHighAccuracyOccupiedBlocks.setSelected(true);
		row2.add(new JLabel("High-Accuracy Occupied:"));
		row2.add(showHighAccuracyOccupiedBlocks);
		
		showHighAccuracyExitedBlocks.setSelected(true);
		row2.add(new JLabel("High-Accuracy Exited:"));
		row2.add(showHighAccuracyExitedBlocks);
		
		showOccupiedAveragedPoint.setSelected(true);
		row2.add(new JLabel("Occupied Point:"));
		row2.add(showOccupiedAveragedPoint);
		
		showEmptyAveragedPoint.setSelected(true);
		row2.add(new JLabel("Empty Point:"));
		row2.add(showEmptyAveragedPoint);
		
		retVal.add(row1);
		retVal.add(row2);
		return retVal;
	}

	public JTextArea getLogsArea() {
		return logsArea;
	}
}
