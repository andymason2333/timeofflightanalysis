package au.com.webglow.voxel.gui;

public class FloorGridDisplayHighAccuracyCell {
	private boolean highlight;
	private int occupiedPointCount = 0;
	private int emptyPointCount = 0;
	
	public FloorGridDisplayHighAccuracyCell() {
		this.highlight = false;
	}
	
	public boolean isHighlight() {
		return highlight;
	}
	
	public void setHighlight(boolean highlight) {
		this.highlight = highlight;
	}

	public int getOccupiedPointCount() {
		return occupiedPointCount;
	}

	public void setOccupiedPointCount(int occupiedPointCount) {
		this.occupiedPointCount = occupiedPointCount;
	}

	public int getEmptyPointCount() {
		return emptyPointCount;
	}

	public void setEmptyPointCount(int emptyPointCount) {
		this.emptyPointCount = emptyPointCount;
	}
}
