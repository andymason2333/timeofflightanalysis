package au.com.webglow.voxel;

import static au.com.webglow.voxel.Constants.GRID_BLOCK_X_COUNT;
import static au.com.webglow.voxel.Constants.GRID_BLOCK_Y_COUNT;
import static au.com.webglow.voxel.Constants.GRID_BLOCK_Z_COUNT;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;

import au.com.webglow.voxel.analyse.units.GridBlock;
import au.com.webglow.voxel.analyse.units.GridBlockPipeBuffer;
import au.com.webglow.voxel.analyse.units.HighAccuracyGridBlock;
import au.com.webglow.voxel.gui.LoggingPanel;

public class PipeGridPositions {
	private List<GridBlockPipeBuffer> occupied = new ArrayList<GridBlockPipeBuffer>();
	private List<GridBlockPipeBuffer> highlightObjectDetected = new ArrayList<GridBlockPipeBuffer>();
	private List<GridBlockPipeBuffer> highlightNewlyOccupied = new ArrayList<GridBlockPipeBuffer>();
//	private List<GridBlockPipeBuffer> highlightHighAccuracyNewlyOccupied = new ArrayList<GridBlockPipeBuffer>();
//	private List<GridBlockPipeBuffer> highlightHighAccuracyNewlyEmpty = new ArrayList<GridBlockPipeBuffer>();
	private List<GridBlockPipeBuffer> highAccuracyOccupied = new ArrayList<GridBlockPipeBuffer>();
//	private List<GridBlockPipeBuffer> highAccuracyExited = new ArrayList<GridBlockPipeBuffer>();
	private GridBlockPipeBuffer occupiedAveragedPoint = null;
	private GridBlockPipeBuffer emptyAveragedPoint = null;
	
	public static PipeGridPositions newInstance(GridBlock[][][] gridBlocks) {
		PipeGridPositions retVal = new PipeGridPositions();
		for (int x = 0; x < GRID_BLOCK_X_COUNT; x++) {
			for (int y = 0; y < GRID_BLOCK_Y_COUNT; y++) {
				for (int z = 0; z < GRID_BLOCK_Z_COUNT; z++) {
					GridBlock gridBlock = gridBlocks[x][y][z];
//					if (gridBlock.getOccupiedMonitor().isObjectDetected()) {
//						retVal.addHighlightObjectDetected(gridBlock);
//					} else 
						
					if (gridBlock.isNewlyOccupied()) {
						retVal.addHighlightNewlyOccupied(gridBlock);
					} else if (gridBlocks[x][y][z].isOccupied() && LoggingPanel.showOccupiedBlocks.isSelected()) {
						retVal.addOccupied(gridBlock);
					}
				}
			}
		}
//		if (highAccuracyRegions != null) {
//			for (HighAccuracyRegion highAccuracyRegion: highAccuracyRegions) {
//				for (int x = highAccuracyRegion.getHighAccuracyGridBlockRange().getStartX(); x <= highAccuracyRegion.getHighAccuracyGridBlockRange().getEndX(); x++) {
//					for (int y = highAccuracyRegion.getHighAccuracyGridBlockRange().getStartY(); y <= highAccuracyRegion.getHighAccuracyGridBlockRange().getEndY(); y++) {
//						for (int z = highAccuracyRegion.getHighAccuracyGridBlockRange().getStartZ(); z <= highAccuracyRegion.getHighAccuracyGridBlockRange().getEndZ(); z++) {
//							if (highAccuracyGridBlocks[x][y][z].getNewlyOccupiedMonitor().isNewlyOccupied()) {
//								retVal.addHighlightHighAccuracyNewlyOccupied(highAccuracyGridBlocks[x][y][z]);
//							}
//							if (highAccuracyGridBlocks[x][y][z].getNewlyOccupiedMonitor().isNewlyEmpty()) {
//								retVal.addHighlightHighAccuracyNewlyEmpty(highAccuracyGridBlocks[x][y][z]);
//							}
//						}
//					}
//				}
//			}	
//		}
		
//		for (int highAccuracyBlockX = 0; highAccuracyBlockX < HIGH_ACCURACY_GRID_BLOCK_X_COUNT; highAccuracyBlockX++) {
//			for (int highAccuracyBlockY = 0; highAccuracyBlockY < HIGH_ACCURACY_GRID_BLOCK_Y_COUNT; highAccuracyBlockY++) {
//				for (int highAccuracyBlockZ = 0; highAccuracyBlockZ < HIGH_ACCURACY_GRID_BLOCK_Z_COUNT; highAccuracyBlockZ++) {
//					if (parentGridBlock.isNewlyOccupied()) {
//						if (highAccuracyGridBlocks[blockX][blockY][blockZ][highAccuracyBlockX][highAccuracyBlockY][highAccuracyBlockZ].isOccupied()) {
//							retVal.addHighAccuracyOccupied(highAccuracyGridBlocks[blockX][blockY][blockZ][highAccuracyBlockX][highAccuracyBlockY][highAccuracyBlockZ]);
//						}
//					}
//				}
//			}
//		}	
		
//		if (highAccuracyObjectRegions != null) {
//			for (HighAccuracyObjectRegion highAccuracyObjectRegion: highAccuracyObjectRegions) {	
//				
//				for (int x = highAccuracyObjectRegion.getGridBlockRange().getStartX(); x <= highAccuracyObjectRegion.getGridBlockRange().getEndX(); x++) {
//					for (int y = highAccuracyObjectRegion.getGridBlockRange().getStartY(); y <= highAccuracyObjectRegion.getGridBlockRange().getEndY(); y++) {
//						for (int z = highAccuracyObjectRegion.getGridBlockRange().getStartZ(); z <= highAccuracyObjectRegion.getGridBlockRange().getEndZ(); z++) {
//							
//							if (condensedGridBlocks[x][y][z].isHighAccuracyNewlyOccupiedDetected()) {
//								for (int highAccX = condensedGridBlocks[x][y][z].getStartHighAccX(); highAccX <= condensedGridBlocks[x][y][z].getEndHighAccX(); highAccX++) {
//									for (int highAccY = condensedGridBlocks[x][y][z].getStartHighAccY(); highAccY <= condensedGridBlocks[x][y][z].getEndHighAccY(); highAccY++) {
//										for (int highAccZ = condensedGridBlocks[x][y][z].getStartHighAccZ(); highAccZ <= condensedGridBlocks[x][y][z].getEndHighAccZ(); highAccZ++) {
//											if (highAccuracyGridBlocks[highAccX][highAccY][highAccZ].getNewlyOccupiedMonitor().isNewlyOccupied()) {
//												retVal.addHighAccuracyOccupied(highAccuracyGridBlocks[highAccX][highAccY][highAccZ]);
//											}
//										}	
//									}
//								}
//							} else if (condensedGridBlocks[x][y][z].isHighAccuracyNewlyEmptyDetected()) {
//								for (int highAccX = condensedGridBlocks[x][y][z].getStartHighAccX(); highAccX <= condensedGridBlocks[x][y][z].getEndHighAccX(); highAccX++) {
//									for (int highAccY = condensedGridBlocks[x][y][z].getStartHighAccY(); highAccY <= condensedGridBlocks[x][y][z].getEndHighAccY(); highAccY++) {
//										for (int highAccZ = condensedGridBlocks[x][y][z].getStartHighAccZ(); highAccZ <= condensedGridBlocks[x][y][z].getEndHighAccZ(); highAccZ++) {
//											if (highAccuracyGridBlocks[highAccX][highAccY][highAccZ].getNewlyOccupiedMonitor().isNewlyEmpty()) {
//												retVal.addHighAccuracyExited(highAccuracyGridBlocks[highAccX][highAccY][highAccZ]);
//											}
//										}	
//									}
//								}
//							}
//						}
//					}
//				}
//				
//				if (highAccuracyObjectRegion.getOccupiedRegressionPoints().size() > 0) {
//					for (Coordinate point: highAccuracyObjectRegion.getOccupiedRegressionPoints()) {
//						retVal.setOccupiedAveragedPoint(highAccuracyGridBlocks[point.getX()][point.getY()][point.getZ()]);	
//					}
//				}
//				
//				if (highAccuracyObjectRegion.getEmptyRegressionPoints().size() > 0) {
//					for (Coordinate point: highAccuracyObjectRegion.getEmptyRegressionPoints()) {
//						retVal.setEmptyAveragedPoint(highAccuracyGridBlocks[point.getX()][point.getY()][point.getZ()]);	
//					}
//				}
//			}		
//		}
		
		return retVal;
	}

	public static void pipeGridPositions(GridBlock[][][] gridBlocks) throws IOException {
		PipeGridPositions pipeGridPositions = PipeGridPositions.newInstance(gridBlocks);
		if (LoggingPanel.showOccupiedBlocks.isSelected()) {
			sendToPipe(pipeGridPositions.getOccupied());
		} else {
			sendToPipe(new ArrayList<GridBlockPipeBuffer>());
		}
		if (LoggingPanel.showNewlyOccupiedBlocks.isSelected()) {
			sendToPipe(pipeGridPositions.getHighlightNewlyOccupied());
		} else {
			sendToPipe(new ArrayList<GridBlockPipeBuffer>());
		}
		if (LoggingPanel.showObjectDetectedBlocks.isSelected()) {
			sendToPipe(pipeGridPositions.getHighlightObjectDetected());
		} else {
			sendToPipe(new ArrayList<GridBlockPipeBuffer>());
		}
		/////
		if (LoggingPanel.showHighAccuracyNewlyOccupiedBlocks.isSelected()) {
			sendToPipe(new ArrayList<GridBlockPipeBuffer>());
		} else {
			sendToPipe(new ArrayList<GridBlockPipeBuffer>());
		}
		/////
		if (LoggingPanel.showHighAccuracyNewlyEmptyBlocks.isSelected()) {
			sendToPipe(new ArrayList<GridBlockPipeBuffer>());
		} else {
			sendToPipe(new ArrayList<GridBlockPipeBuffer>());
		}
		if (LoggingPanel.showHighAccuracyOccupiedBlocks.isSelected()) {
			sendToPipe(pipeGridPositions.getHighAccuracyOccupied());
		} else {
			sendToPipe(new ArrayList<GridBlockPipeBuffer>());
		}
		/////
		if (LoggingPanel.showHighAccuracyExitedBlocks.isSelected()) {
			sendToPipe(new ArrayList<GridBlockPipeBuffer>());
		} else {
			sendToPipe(new ArrayList<GridBlockPipeBuffer>());
		}
		if (LoggingPanel.showOccupiedAveragedPoint.isSelected()) {
			sendPointToPipe(pipeGridPositions.getOccupiedAveragedPoint());
		} else {
			sendPointToPipe(null);
		}
		if (LoggingPanel.showEmptyAveragedPoint.isSelected()) {
			sendPointToPipe(pipeGridPositions.getEmptyAveragedPoint());
		} else {
			sendPointToPipe(null);
		}
	}

	private static void sendToPipe(List<GridBlockPipeBuffer> positions) throws IOException {		
		byte[] positionsCount = new byte[2];
		
		ByteBuffer positionsCountBuffer = ByteBuffer.allocate(4);
		positionsCountBuffer.order(ByteOrder.BIG_ENDIAN);
		positionsCountBuffer.putInt(positions.size());
		byte[] positionsCountBufferArr = positionsCountBuffer.array();
		positionsCount[0] = positionsCountBufferArr[2];
		positionsCount[1] = positionsCountBufferArr[3];
				
		ByteBuffer positionsBuffer = ByteBuffer.allocate(positions.size() * 6);		
		for (int i = 0; i < positions.size(); i++) {
			positionsBuffer.put(positions.get(i).getPipeBuffer());
		}
				
		System.out.write(positionsCount);
		System.out.write(positionsBuffer.array());		
	}
	
	private static void sendPointToPipe(GridBlockPipeBuffer position) throws IOException {		
		byte[] positionsCount = new byte[2];
		
		ByteBuffer positionsCountBuffer = ByteBuffer.allocate(4);
		positionsCountBuffer.order(ByteOrder.BIG_ENDIAN);
		positionsCountBuffer.putInt(position != null ? 1 : 0);
		byte[] positionsCountBufferArr = positionsCountBuffer.array();
		positionsCount[0] = positionsCountBufferArr[2];
		positionsCount[1] = positionsCountBufferArr[3];
		
		System.out.write(positionsCount);
			
		if (position != null) {
			ByteBuffer positionsBuffer = ByteBuffer.allocate(6);		
			positionsBuffer.put(position.getPipeBuffer());
			
			System.out.write(positionsBuffer.array());
		}		
	}
	
//	private static void sendLinesToPipe(List<LinePipeBuffer> lines) throws IOException {		
//		byte[] linesCount = new byte[2];
//		
//		ByteBuffer linesCountBuffer = ByteBuffer.allocate(4);
//		linesCountBuffer.order(ByteOrder.BIG_ENDIAN);
//		linesCountBuffer.putInt(lines.size());
//		byte[] linesCountBufferArr = linesCountBuffer.array();
//		linesCount[0] = linesCountBufferArr[2];
//		linesCount[1] = linesCountBufferArr[3];
//				
//		ByteBuffer linesBuffer = ByteBuffer.allocate(lines.size() * 12);		
//		for (int i = 0; i < lines.size(); i++) {
//			linesBuffer.put(lines.get(i).getPipeBuffer());
//		}
//
//		System.out.write(linesCount);
//		System.out.write(linesBuffer.array());		
//	}
	
	public void addOccupied(GridBlock gridBlock) {
		occupied.add(gridBlock);
	}
	
	public void addHighlightObjectDetected(GridBlock gridBlock) {
		highlightObjectDetected.add(gridBlock);
	}
	
	public void addHighlightNewlyOccupied(GridBlock gridBlock) {
		highlightNewlyOccupied.add(gridBlock);
	}
	
	public void addHighAccuracyOccupied(HighAccuracyGridBlock highAccuracyGridBlock) {
		highAccuracyOccupied.add(highAccuracyGridBlock);
	}
	
//	public void addHighAccuracyExited(HighAccuracyGridBlock highAccuracyGridBlock) {
//		highAccuracyExited.add(highAccuracyGridBlock);
//	}
//	
//	public void addHighlightHighAccuracyNewlyOccupied(HighAccuracyGridBlock highAccuracyGridBlock) {
//		highlightHighAccuracyNewlyOccupied.add(highAccuracyGridBlock);
//	}
//	
//	public void addHighlightHighAccuracyNewlyEmpty(HighAccuracyGridBlock highAccuracyGridBlock) {
//		highlightHighAccuracyNewlyEmpty.add(highAccuracyGridBlock);
//	}
	
	public List<GridBlockPipeBuffer> getOccupied() {
		return occupied;
	}

	public List<GridBlockPipeBuffer> getHighlightObjectDetected() {
		return highlightObjectDetected;
	}

	public List<GridBlockPipeBuffer> getHighlightNewlyOccupied() {
		return highlightNewlyOccupied;
	}

	public List<GridBlockPipeBuffer> getHighAccuracyOccupied() {
		return highAccuracyOccupied;
	}

//	public List<GridBlockPipeBuffer> getHighAccuracyExited() {
//		return highAccuracyExited;
//	}
//
//	public List<GridBlockPipeBuffer> getHighlightHighAccuracyNewlyOccupied() {
//		return highlightHighAccuracyNewlyOccupied;
//	}
//	
//	public List<GridBlockPipeBuffer> getHighlightHighAccuracyNewlyEmpty() {
//		return highlightHighAccuracyNewlyEmpty;
//	}

	public GridBlockPipeBuffer getOccupiedAveragedPoint() {
		return occupiedAveragedPoint;
	}

	public void setOccupiedAveragedPoint(GridBlockPipeBuffer occupiedAveragedPoint) {
		this.occupiedAveragedPoint = occupiedAveragedPoint;
	}

	public GridBlockPipeBuffer getEmptyAveragedPoint() {
		return emptyAveragedPoint;
	}

	public void setEmptyAveragedPoint(GridBlockPipeBuffer emptyAveragedPoint) {
		this.emptyAveragedPoint = emptyAveragedPoint;
	}
}