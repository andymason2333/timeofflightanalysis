package au.com.webglow.voxel;

import java.io.DataInputStream;
import java.io.IOException;

import au.com.webglow.voxel.analyse.FrameAnalyser;
import au.com.webglow.voxel.gui.DebugConsoleFrame;

public class StreamDataReader {		
	private FrameAnalyser frameAnalyser;
	private DataInputStream in = new DataInputStream(System.in);
	private byte[] frame = new byte[153600];
	
	public static StreamDataReader newBufferReadingInstance(FrameAnalyser frameAnalyser) {
		StreamDataReader retVal = new StreamDataReader(frameAnalyser);
		return retVal;
	}
	
	private StreamDataReader(FrameAnalyser frameAnalyser) {
		this.frameAnalyser = frameAnalyser;
	}
	
	public void beginReading() {
        try {
            while (true) {
            	try {
            		in.readFully(frame);
            		frameAnalyser.processFrame(frame);
            	} catch (Exception e) {
            		DebugConsoleFrame.appendLogTextLn(e);
            	}
            	
            }

        } finally {
            if (in != null) {
                try {
                	in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
	}
}
