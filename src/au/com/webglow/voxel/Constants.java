package au.com.webglow.voxel;

public class Constants {
	public static final int FRAMES_PER_SECOND = 30;
	public static final int COMPARISON_FRAME_COUNT = 1;
	public static final int PIPE_POSITIONS_FRAME_COUNT = 2;
	public static final int DETERMINE_FLOOR_POSITION_FRAME_COUNT = 10;
	
	public static final int SENSOR_ROW_SIZE = 240;
	public static final int SENSOR_COL_SIZE = 320;
	
	public static final int DEPTH_COUNT = 40;
	public static final int DEPTH_BLOCK_LENGTH = 20;
	public static final double DEPTH_SCALE = ((double)1 / DEPTH_BLOCK_LENGTH);

	public final static int GRID_BLOCK_X_COUNT = 40;
	public final static int GRID_BLOCK_Y_COUNT = 30;
	public final static int GRID_BLOCK_Z_COUNT = 40;
	public final static int GRID_CM_PER_BLOCK = 20;
	
	public static final int HIGH_ACCURACY_DEPTH_COUNT = 160;
	public static final int HIGH_ACCURACY_DEPTH_BLOCK_LENGTH = 5;
	public static final double HIGH_ACCURACY_DEPTH_SCALE = ((double)1 / HIGH_ACCURACY_DEPTH_BLOCK_LENGTH);
	
	public final static int HIGH_ACCURACY_GRID_BLOCK_X_COUNT = 160;
	public final static int HIGH_ACCURACY_GRID_BLOCK_Y_COUNT = 120;
	public final static int HIGH_ACCURACY_GRID_BLOCK_Z_COUNT = 160;
	public final static int HIGH_ACCURACY_GRID_CM_PER_BLOCK = 5;
	public static final int HIGH_ACCURACY_PER_GRID_BLOCK = GRID_CM_PER_BLOCK / HIGH_ACCURACY_GRID_CM_PER_BLOCK;
	
	public static final int FLOOR_GRID_CELL_COL_COUNT = GRID_BLOCK_X_COUNT;
	public static final int FLOOR_GRID_CELL_DEPTH_COUNT = GRID_BLOCK_Z_COUNT;
	
	public static final int HIGH_ACCURACY_FLOOR_GRID_CELL_COL_COUNT = HIGH_ACCURACY_GRID_BLOCK_X_COUNT;
	public static final int HIGH_ACCURACY_FLOOR_GRID_CELL_DEPTH_COUNT = HIGH_ACCURACY_GRID_BLOCK_Z_COUNT;
	
	public static final int OCCUPIED_CONDENSED_SIZE = 2;
	public static final int OCCUPIED_CONDENSED_X_COUNT = GRID_BLOCK_X_COUNT-(OCCUPIED_CONDENSED_SIZE-1);
	public static final int OCCUPIED_CONDENSED_Y_COUNT = GRID_BLOCK_Y_COUNT-(OCCUPIED_CONDENSED_SIZE-1);
	public static final int OCCUPIED_CONDENSED_Z_COUNT = GRID_BLOCK_Z_COUNT-(OCCUPIED_CONDENSED_SIZE-1);
}
